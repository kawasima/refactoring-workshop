# リファクタリングワークショップ

## 環境セットアップ

1. clone後、VS Codeで開きます。
2. VS Codeに、Remote Containerプラグインが入っていることを確認してください。
3. VS Codeの左下の「><」をクリックし、Reopen Containerを選択します。

これでRemoteコンテナで起動し、npm installまで実行されます。

## テストの起動

必要に応じて、フィルタして実行します。

### サーキットブレイカー

```
npm run test:watch -- --testPathPattern=circuit_breaker
```

### ワークフロー

```
npm run test:watch -- --testPathPattern=workflow
```