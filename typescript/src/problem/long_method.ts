// Replace Primitive with Object
// Decompose Conditional
// Replace Conditional with Polymorphism

import { isHoliday } from "japanese-holidays"

type RouteType = 'RURAL' | 'URBAN'
type VehicleFamily = 'STANDARD' | 'MINI' | 'MOTORCYCLE' | 'OTHER'
type Driver = {
  countPerMonth: number;
}

export type HighwayDrive = {
  enteredAt: Date;
  exitedAt: Date;
  driver: Driver;
  routeType: RouteType;
  vehicleFamily: VehicleFamily;
}

export function calc(drive: HighwayDrive): number {
  if (drive.exitedAt.getTime() < drive.enteredAt.getTime()) {
    throw "おかしい";
  }
  if (drive.exitedAt.getTime() - drive.enteredAt.getTime() > 24 * 60 * 60) {
    throw "1日以上は運転しすぎ"
  }

  let morningEnd: Date, morningStart: Date;
  if (drive.enteredAt.getHours() >= 9) {
    morningStart = new Date(drive.enteredAt.getTime() + 24 * 60 * 60);
    morningStart.setHours(6);
    morningStart.setMinutes(0);
    morningStart.setSeconds(0);
    morningEnd = new Date(drive.enteredAt.getTime() + 24 * 60 * 60);
    morningEnd.setHours(9);
    morningEnd.setMinutes(0);
    morningEnd.setSeconds(0);
  } else {
    morningStart = new Date(drive.enteredAt.getTime());
    morningStart.setHours(6);
    morningStart.setMinutes(0);
    morningStart.setSeconds(0);
    morningEnd = new Date(drive.enteredAt.getTime());
    morningEnd.setHours(9);
    morningEnd.setMinutes(0);
    morningEnd.setSeconds(0);
  }

  let eveningEnd: Date, eveningStart: Date;
  if (drive.enteredAt.getHours() >= 20) {
    eveningStart = new Date(drive.enteredAt.getTime() + 24 * 60 * 60);
    eveningStart.setHours(17);
    eveningStart.setMinutes(0);
    eveningStart.setSeconds(0);
    eveningEnd = new Date(drive.enteredAt.getTime() + 24 * 60 * 60);
    eveningEnd.setHours(20);
    eveningEnd.setMinutes(0);
    eveningEnd.setSeconds(0);
  } else {
    eveningStart = new Date(drive.enteredAt.getTime());
    eveningStart.setHours(17);
    eveningStart.setMinutes(0);
    eveningStart.setSeconds(0);
    eveningEnd = new Date(drive.enteredAt.getTime());
    eveningEnd.setHours(20);
    eveningEnd.setMinutes(0);
    eveningEnd.setSeconds(0);
  }

  let midnightEnd: Date, midnightStart: Date;
  if (drive.enteredAt.getHours() >= 4) {
    midnightStart = new Date(drive.enteredAt.getTime() + 24 * 60 * 60);
    midnightStart.setHours(0);
    midnightStart.setMinutes(0);
    midnightStart.setSeconds(0);
    midnightEnd = new Date(drive.enteredAt.getTime() + 24 * 60 * 60);
    midnightEnd.setHours(4);
    midnightEnd.setMinutes(0);
    midnightEnd.setSeconds(0);
  } else {
    midnightStart = new Date(drive.enteredAt.getTime());
    midnightStart.setHours(0);
    midnightStart.setMinutes(0);
    midnightStart.setSeconds(0);
    midnightEnd = new Date(drive.enteredAt.getTime());
    midnightEnd.setHours(4);
    midnightEnd.setMinutes(0);
    midnightEnd.setSeconds(0);
  }

  // 平日朝夕方割引
  if (!isHoliday(morningStart)
    && !isHoliday(eveningStart)
    && ((drive.enteredAt.getTime() <= morningEnd.getTime() && drive.exitedAt.getTime() <= morningStart.getTime())
      || (drive.enteredAt.getTime() <= eveningEnd.getTime() && drive.exitedAt.getTime() >= eveningStart.getTime()))
    && drive.routeType === 'RURAL'
  ) {
    if (drive.driver.countPerMonth >= 10) {
      return 50;
    }
    if (drive.driver.countPerMonth >= 5) {
      return 30;
    }
  } else {
    //休日割引
    if ((isHoliday(drive.enteredAt)
      || isHoliday(drive.exitedAt))
      && ['STANDARD', 'MINI', 'MOTORCYCLE'].includes(drive.vehicleFamily)
      && drive.routeType === 'RURAL') {
      return 30;
    } else {
      // 深夜割引
      if (drive.enteredAt.getTime() <= midnightEnd.getTime() && drive.enteredAt.getTime() >= midnightStart.getTime()) {
        return 30;
      } else {
        return 0;
      }
    }
  }
  return 0;
}

export namespace Nonsense {
  export class Period {
    start: Date;
    end: Date;

    constructor(start: Date, end: Date) {
      this.start = start;
      this.end = end;
    }
  }

  function applesause(drive: HighwayDrive, startHour: number, endHour: number): Period {
    if (drive.enteredAt.getHours() >= endHour) {
      const start = new Date(drive.enteredAt.getTime() + 24 * 60 * 60);
      start.setHours(startHour);
      start.setMinutes(0);
      start.setSeconds(0);
      const end = new Date(drive.enteredAt.getTime() + 24 * 60 * 60);
      end.setHours(endHour);
      end.setMinutes(0);
      end.setSeconds(0);
      return new Period(start, end);
    } else {
      const start = new Date(drive.enteredAt.getTime());
      start.setHours(startHour);
      start.setMinutes(0);
      start.setSeconds(0);
      const end = new Date(drive.enteredAt.getTime());
      end.setHours(endHour);
      end.setMinutes(0);
      end.setSeconds(0);
      return new Period(start, end);
    }
  }
  export function calc(drive: HighwayDrive): number {
    const morningPeriod = applesause(drive, 6, 9);
    const eveningPeriod = applesause(drive, 17, 20);
    const midnightPeriod = applesause(drive, 0, 4);
    // 平日朝夕方割引
    if (!isHoliday(morningPeriod.start)
      && !isHoliday(eveningPeriod.start)
      && ((drive.enteredAt.getTime() <= morningPeriod.end.getTime() && drive.exitedAt.getTime() <= morningPeriod.start.getTime())
        || (drive.enteredAt.getTime() <= eveningPeriod.end.getTime() && drive.exitedAt.getTime() >= eveningPeriod.start.getTime()))
      && drive.routeType === 'RURAL'
    ) {
      if (drive.driver.countPerMonth >= 10) {
        return 50;
      }
      if (drive.driver.countPerMonth >= 5) {
        return 30;
      }
    } else {
      //休日割引
      if ((isHoliday(drive.enteredAt)
        || isHoliday(drive.exitedAt))
        && ['STANDARD', 'MINI', 'MOTORCYCLE'].includes(drive.vehicleFamily)
        && drive.routeType === 'RURAL') {
        return 30;
      } else {
        // 深夜割引
        if (drive.enteredAt.getTime() <= midnightPeriod.end.getTime() && drive.enteredAt.getTime() >= midnightPeriod.start.getTime()) {
          return 30;
        } else {
          return 0;
        }
      }
    }
    return 0;
  }
}

export namespace Honest {
  function probably_calcPeriodFromSomeArguments_AndStuff(drive: HighwayDrive, startHour: number, endHour: number): Nonsense.Period {
    // 略
    return null
  }
}

export namespace HonestAndComplete {
  function calcPeriodAndIfEnteredAtISAfterTheEndHourThenPlusOneDay(drive: HighwayDrive, startHour: number, endHour: number): Nonsense.Period {
    // 略
    return null;
  }
}

// 完全な名前から複数責務が混ざっていることを識別し、
// 分離したり、カプセル化したりする。
export namespace DoesTheRightThing {
  export class TimePeriod {
    start: number;
    end: number;

    constructor(start: number, end: number) {
      if (!Number.isInteger(start) || start < 0 || start > 23) {
        throw `start is not hour`
      }
      if (!Number.isInteger(end) || end < 0 || end > 23) {
        throw `end is not hour`
      }
      if (start > end) {
        throw `{start} must be before {end}`
      }
      this.start = start;
      this.end = end;
    }
  }

  function normalizeDate(d: Date): Date {
    const date = new Date(d);
    date.setHours(0);
    date.setMinutes(0);
    date.setSeconds(0);
    date.setMilliseconds(0);
    return date;
  }

  export class DateTimePeriod {
    start: Date;
    end: Date;

    constructor(standardDateTime: Date, timePeriod: TimePeriod) {
      const normalizedStdDateTime = normalizeDate(standardDateTime);
      if (normalizedStdDateTime.getHours() >= timePeriod.start) {
        normalizedStdDateTime.setDate(normalizedStdDateTime.getDate() + 1);
      }
      this.start = new Date(normalizedStdDateTime.setHours(timePeriod.start));
      this.end = new Date(normalizedStdDateTime.setHours(timePeriod.end));
    }
  }
  function calcPeriod(drive: HighwayDrive, startHour: number, endHour: number): DateTimePeriod {
    const timePeriod = new TimePeriod(startHour, endHour);
    return new DateTimePeriod(drive.enteredAt, timePeriod);
  }
}

// なぜ期間を計算するのか?
// -> 通行期間が時間帯に入っているかどうかを判定したい
// 呼び出し側から間違った呼び出しをしてしまわないように、名前に「意図」を込める
export namespace Intent {
  export function createAppliedDiscountPeriod(startHour: number, endHour: number, drive: HighwayDrive): DoesTheRightThing.DateTimePeriod {
    const timePeriod = new DoesTheRightThing.TimePeriod(startHour, endHour);
    return new DoesTheRightThing.DateTimePeriod(drive.enteredAt, timePeriod);
  }

  function calc(drive: HighwayDrive) {
    const morningPeriod = createAppliedDiscountPeriod(6, 9, drive);
    const eveningPeriod = createAppliedDiscountPeriod(17, 20, drive);
    const midnightPeriod = createAppliedDiscountPeriod(0, 4, drive);

    // ... 以下略
  }
}

// 不変条件やふるまいが違うものには、異なる型を与えていく
export namespace DomainAbstraction {
  class Percentage {
    private value: number;
    constructor(value: number) {
      if (value < 0 || value > 100) {
        throw `value is out of range`;
      }
      this.value = value;
    }
    greaterThan(other: Percentage): boolean {
      return this.value > other.value;
    }
    asNumber(): number {
      return this.value;
    }
  }

  abstract class DiscountRule {
    abstract isApplicable(drive: HighwayDrive): boolean;
    abstract discountPercentage(drive: HighwayDrive): Percentage;
  }

  class DiscountInMorningOrEvening extends DiscountRule {
    morningPeriodGenerator = Intent.createAppliedDiscountPeriod.bind(null, 0, 4);
    eveningPeriodGenerator = Intent.createAppliedDiscountPeriod.bind(null, 17, 20);

    isApplicable(drive: HighwayDrive): boolean {
      const morningPeriod = this.morningPeriodGenerator.call(null, drive);
      const eveningPeriod = this.eveningPeriodGenerator.call(null, drive);
      return (!isHoliday(morningPeriod.start) && drive.routeType === 'RURAL');

    }

    discountPercentage(drive: HighwayDrive): Percentage {
      const count = drive.driver.countPerMonth;
      if (count > 10) {
        return new Percentage(50);
      } else if (count >= 5) {
        return new Percentage(30);
      } else {
        return new Percentage(0);
      }
    }
  }

  const discountRules = [
    new DiscountInMorningOrEvening(),
  ];
  function calc(drive: HighwayDrive): Percentage {
    return discountRules
      .filter(rule => rule.isApplicable(drive))
      .map(rule => rule.discountPercentage(drive))
      .reduce((a, b) => a.greaterThan(b) ? a : b, new Percentage(0))
  }
}
