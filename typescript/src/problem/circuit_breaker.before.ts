
export class CircuitBreakerOpenException extends Error {
}

export class CircuitBreaker {
  status: string;
  errorThreshold: number;
  successThreshold: number;
  errorCounter: number;
  successCounter: number;
  closeTimer: NodeJS.Timeout;
  openedAt: Date;
  openToHalfOpenWaitTime: number;

  constructor() {
    this.status = 'close';
    this.errorThreshold = 3;
    this.errorCounter = 0;
    this.successThreshold = 3;
    this.successCounter = 0;
    this.openToHalfOpenWaitTime = 1000;
    this.closeTimer = setInterval(() => {
      this.errorCounter = 0;
    }, 1000);
  }

  executeAction(action: () => void): void {
    if (this.status === 'close') {
      try {
        action();
      } catch (e) {
        this.errorCounter += 1;
        if (this.errorCounter >= this.errorThreshold) {
          this.status = 'open';
          this.openedAt = new Date();
        }
        throw e;
      }
    } else if (this.status === 'open') {
      if (new Date().getTime() - this.openedAt.getTime() > this.openToHalfOpenWaitTime) {
        this.status = 'half_open';
      }
      throw new CircuitBreakerOpenException();
    } else if (this.status === 'half_open') {
      try {
        action();
        this.successCounter += 1;
        if (this.successCounter >= this.successThreshold) {
          this.status = 'close';
        }
      } catch (e) {
        this.successCounter = 0;
        this.status = 'open';
        this.openedAt = new Date();
        throw e;
      }
    }
  }
}

