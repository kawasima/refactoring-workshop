type Contact = {
    firstName: string;
    middleInitial?: string;
    lastName: string;
    emailAddress: string;
    isEmailVerified: boolean;
    address1: string;
    address2?: string;
    city: string;
    state: string;
    zip: string;
    isAddressValid: boolean;
}

function sendEmail(emailAddress : string) : void {

}

export function sendMessage(contact: Contact) : void {
    if (contact.zip && contact.state) {

    } else if (contact.emailAddress && contact.isEmailVerified) {
        sendEmail(contact.emailAddress);
    }
}