import { calc, HighwayDrive } from "./long_method";

describe("test", () => {
    it("0%", () => {
        const drive: HighwayDrive = {
            driver: {
                countPerMonth: 1
            },
            routeType: 'URBAN',
            vehicleFamily: 'STANDARD',
            enteredAt: new Date(),
            exitedAt: new Date()
        }
        console.log(calc(drive));
    });
});
