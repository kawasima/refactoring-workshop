import { Cart, CartItem, User, CartPricer, SKU, TaxManager } from "./feature_envy";

const taxManager = new TaxManager();
taxManager.add('JP', 1.10);
taxManager.add('FR', 1.20);
const user1 = new User({ userId: '1', country: 'JP' });
const apple = new SKU({ skuId: '1', name: 'Apple', price: 100 });
const orange = new SKU({ skuId: '2', name: 'Orange', price: 30 });

describe('test', () => {
    it('calcurate tax', () => {
        const cart = new Cart({ user: user1 });
        cart.add(new CartItem({ sku: apple, amount: 3 }));
        cart.add(new CartItem({ sku: orange, amount: 4 }));
        console.log(new CartPricer({ taxManager }).total(cart));
    });

});
