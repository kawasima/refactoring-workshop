export class Employee {
  id: string;
  position: string;

  constructor(id: string, position: string) {
    this.id = id;
    this.position = position;
  }

  equals(other: Employee) {
    return other.id === this.id;
  }
}

/**
 * 購買申請
 */
export class Requisition {
  /** 承認ステータス */
  requisitionStatus: string;
  /** 承認ルート */
  approveRoute: Employee[];
  /** 購入商品 */
  requisitionLines: { price: number }[];
  /** 承認履歴 */
  approveHistory: {
    isDeclined: boolean;
    approver: Employee;
    approvedAt: Date;
  }[];

  constructor(requisitionLines: { price: number }[],
    approveRoute: Employee[]) {
    this.requisitionLines = requisitionLines;
    this.approveRoute = approveRoute;
    this.approveHistory = [];
    this.requisitionStatus = 'APPLIED';
  }

  checkRoute(): void {
    const total = this.requisitionLines.reduce((prev, cur) => prev + cur.price, 0);
    if (total <= 200000) {
      // 課長承認
      if (this.approveRoute.slice(-1)[0].position !== '課長') {
        throw `この申請は課長の最終承認が必要です`;
      }
    } else if (total <= 1000000) {
      // 部長承認
      if (this.approveRoute.slice(-1)[0].position !== '部長') {
        throw `この申請は部長の最終承認が必要です`;
      }
    } else {
      // 社長承認
      if (this.approveRoute.slice(-1)[0].position !== '社長') {
        throw `この申請は社長の最終承認が必要です`;
      }
    }
  }

  // 現在のワークフローステータスを返す
  getStatus(): string {
    if (this.requisitionStatus === 'APPLIED' || this.requisitionStatus == 'DECLINED') {
      return this.requisitionStatus;
    }
    const lastHistory = this.approveHistory.slice(-1)[0];
    const routeIndex = this.approveRoute.findIndex(u => u === lastHistory.approver);
    if (routeIndex >= this.approveRoute.length - 1) {
      return 'FINAL_ACCEPTED';
    }
    return this.requisitionStatus;
  }

  // 購入申請内容を承認または差し戻しします
  proceedWorkflow(approver: Employee, isDeclined: boolean) {
    if (isDeclined && this.requisitionStatus === 'DECLINED') {
      throw `すでに差し戻されている申請です`;
    }
    const lastHistory = this.approveHistory.slice(-1)[0];
    const routeIndex = this.approveRoute.findIndex(u => lastHistory && u === lastHistory.approver);
    if (this.requisitionStatus === 'ACCEPTED' && routeIndex >= this.approveRoute.length - 1) {
      throw `申請はすでに最終承認されています`;
    }
    // 直近の承認ルートが否認されていたら
    if (lastHistory && lastHistory.isDeclined) {
      if (isDeclined) {
        throw `すでに否認された申請です`;
      } if (!approver.equals(this.approveRoute[0])) {
        throw `承認者が承認ルートで設定されたものと違います`;
      }
    } else {
      if (!approver.equals(this.approveRoute[routeIndex + 1])) {
        throw `承認者が承認ルートで設定されたものと違います`;
      }
    }
    this.requisitionStatus = isDeclined ? 'DECLINED' : 'ACCEPTED';
    this.approveHistory.push({
      isDeclined,
      approver: approver,
      approvedAt: new Date()
    });
  }
}
