export class User {
  userId: string;
  country: string;

  constructor({ userId, country }: { userId: string, country: string }) {
    this.userId = userId;
    this.country = country;
  }
}

export class TaxManager {
  taxByCountry: Map<string, number>;

  constructor() {
    this.taxByCountry = new Map();
  }

  add(country: string, rate: number): void {
    this.taxByCountry.set(country, rate);
  }

  getRate(country: string): number {
    return this.taxByCountry.get(country);
  }
}

export class SKU {
  skuId: string;
  name: string;
  price: number;

  constructor({ skuId, name, price }: { skuId: string, name: string, price: number }) {
    this.skuId = skuId
    this.name = name;
    this.price = price;
  }
}

export class CartItem {
  sku: SKU;
  amount: number;

  constructor({ sku, amount }: { sku: SKU, amount: number }) {
    this.sku = sku;
    this.amount = amount;
  }
}

export class Cart {
  user: User;
  items: CartItem[];

  constructor({ user }: { user: User }) {
    this.user = user;
    this.items = [];
  }

  add({ sku, amount }: { sku: SKU, amount: number }): void {
    this.items.push(new CartItem({ sku: sku, amount: amount }));
  }
}

export class CartPricer {
  taxManager: TaxManager;

  constructor({ taxManager }: { taxManager: TaxManager }) {
    this.taxManager = taxManager;
  }

  total(cart: Cart): number {
    return cart.items
      .map(item => item.amount * (item.sku.price * this.taxManager.getRate(cart.user.country)))
      .reduce((prev: number, cur: number): number => prev + cur);
  }
}
