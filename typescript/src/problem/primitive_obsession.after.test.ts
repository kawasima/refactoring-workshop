import { Contact, Email, Name, PostalAddress, Tel } from "./primitive_obsession.after";

describe('After', () => {
    it('Invalid Name', () => {
        expect(() => new Contact(new Name('John', undefined))).toThrow(/Invalid Name/);
    });

    it('Valid contact', () => {
        expect(() => new Contact(new Name('John', 'Doe'))).not.toThrow();
    })
    it('send email', () => {
        const contact = new Contact(new Name('John', 'Doe'), {
            email: new Email('abc@example.com', true)
        });
        expect(contact.sendMessage('Hello')).toContain(contact.email?.emailAddress);
    })
    it('send postal', () => {
        const contact = new Contact(new Name('John', 'Doe'), {
            postalAddress: new PostalAddress('12345', 'NY', 'New York', '123 Main St', 'Empire State Building')
        });
        expect(contact.sendMessage('Hello')).toContain(contact.postalAddress?.postalCode);
    })
    it('send tel', () => {
        const contact = new Contact(new Name('John', 'Doe'), {
            tel: new Tel('123-456-7890')
        });
        expect(contact.sendMessage('Hello')).toContain(contact.tel?.telNo);
    })
})
