import { Requisition, Employee, EmployeeId, RequisitionLine, RequisitionLines, Price, Positions, ApproveRoute, AppliedState, DeclinedState, FinalApprovedState } from "./improved_workflow";

const 主任 = new Employee("1" as EmployeeId, Positions.主任);
const 課長 = new Employee("2" as EmployeeId, Positions.課長);
const 部長 = new Employee("3" as EmployeeId, Positions.部長);
const 社長 = new Employee("4" as EmployeeId, Positions.社長);

describe('課長承認', () => {
  it('課長承認で課長が一度差し戻す', () => {
    const requisition = new Requisition(
      new RequisitionLines([new RequisitionLine(new Price(15000))]),
      new ApproveRoute([主任, 課長]));

    requisition.applied();
    let err;
    err = requisition.approve(主任);
    if (err != null) {
      throw err;
    }
    expect(requisition.requisitionState).toBeInstanceOf(AppliedState);
    requisition.decline(課長);
    expect(requisition.requisitionState).toBeInstanceOf(DeclinedState);
    requisition.applied();
    requisition.approve(主任);
    expect(requisition.requisitionState).toBeInstanceOf(AppliedState);
    requisition.approve(課長);
    expect(requisition.requisitionState).toBeInstanceOf(FinalApprovedState);
  });
});

describe('部長承認', () => {
  it('30万円の決裁は部長承認', () => {
    expect(() => new Requisition(new RequisitionLines([
      new RequisitionLine(new Price(290000)),
      new RequisitionLine(new Price(10000)),
    ]), new ApproveRoute([主任, 課長]))).toThrow("この申請は部長の最終承認が必要です");
  });
});

describe('社長承認', () => {
  it('100万円超の決裁は社長承認', () => {
    expect(() => new Requisition(new RequisitionLines([
      new RequisitionLine(new Price(300000)),
      new RequisitionLine(new Price(300000)),
      new RequisitionLine(new Price(400001)),
    ]), new ApproveRoute([主任, 課長, 部長]))).toThrow("この申請は社長の最終承認が必要です");
  });
});
