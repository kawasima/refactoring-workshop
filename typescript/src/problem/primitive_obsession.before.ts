export class Contact {
  firstName?: string;
  middleInitial?: string;
  lastName?: string;
  emailAddress?: string;
  isEmailVerified: boolean = false;
  postalCode?: string;
  prefectureCd?: string;
  city?: string;
  street?: string;
  building?: string;
  telNo?: string;

  validate() {
    if (this.firstName === undefined || this.lastName === undefined) {
      throw new Error('Invalid Name');
    }
    if ((this.postalCode !== undefined && (this.prefectureCd === undefined || this.city === undefined || this.street === undefined))
      || (this.prefectureCd !== undefined && (this.postalCode === undefined || this.city === undefined || this.street === undefined))
      || (this.city !== undefined && (this.postalCode === undefined || this.prefectureCd === undefined || this.street === undefined))
      || (this.street !== undefined && (this.postalCode === undefined || this.prefectureCd === undefined || this.city === undefined))) {
      throw new Error('Invalid Address');
    }
  }
  sendMessage(message: string): string {
    if (this.emailAddress !== undefined && this.isEmailVerified) {
      return `send ${message} to ${this.emailAddress}`
    } else if (this.postalCode !== undefined) {
      return `send ${message} to ${this.postalCode} ${this.prefectureCd} ${this.city} ${this.street} ${this.building}`
    } else if (this.telNo !== undefined) {
      return `send ${message} to ${this.telNo}`
    } else {
      throw new Error('Cannot contact')
    }
  }
}

