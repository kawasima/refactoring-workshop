import { CircuitBreaker } from "./circuit_breaker.before";

describe('Open circuit breaker', () => {
    it('random errors', async () => {
        const circuitBreaker = new CircuitBreaker();
        while(true) {
            try {
                await new Promise(resolve => setTimeout(resolve, 100));
                circuitBreaker.executeAction(() => {
                    if (Math.random() > .5) {
                        throw Error('error');
                    }
                });
            } catch (e) {
            }
            if (circuitBreaker.status === "open")
                break;
        }
        
        expect(circuitBreaker.status).toBe("open");
        while(true) {
            try {
                await new Promise(resolve => setTimeout(resolve, 100));
                circuitBreaker.executeAction(() => {
                    if (Math.random() > .5) {
                        throw Error('error');
                    }
                });
            } catch (e) {
            }
            if (circuitBreaker.status === String("half_open")) {
                break;
            }
        }
        expect(circuitBreaker.status).toBe("half_open");

        while(true) {
            try {
                await new Promise(resolve => setTimeout(resolve, 100));
                circuitBreaker.executeAction(() => {

                });
            } catch (e) {
            }
            if (circuitBreaker.status === String("close")) {
                break;
            }
        }

        clearInterval(circuitBreaker.closeTimer);
    });
});

