import { Requisition, Employee } from "./workflow";

const 主任 = new Employee("1", "主任");
const 課長 = new Employee("2", "課長");
const 部長 = new Employee("3", "部長");
const 社長 = new Employee("4", "社長");

describe('課長承認', () => {
  it('課長承認で課長が一度差し戻す', () => {
    const requisition = new Requisition([
      { price: 15000 }
    ], [主任, 課長]);

    requisition.checkRoute();
    expect(requisition.getStatus()).toBe('APPLIED');

    requisition.proceedWorkflow(主任, false);
    expect(requisition.getStatus()).toBe('ACCEPTED');
    requisition.proceedWorkflow(課長, true);
    expect(requisition.getStatus()).toBe('DECLINED');
    requisition.proceedWorkflow(主任, false);
    expect(requisition.getStatus()).toBe('ACCEPTED');
    requisition.proceedWorkflow(課長, false);
    expect(requisition.getStatus()).toBe('FINAL_ACCEPTED');
  });
});

describe('部長承認', () => {
  it('30万円の決裁は部長承認', () => {
    const requisition = new Requisition([
      { price: 290000 },
      { price: 10000 }
    ], [主任, 課長]);
    expect(() => requisition.checkRoute()).toThrow("この申請は部長の最終承認が必要です");;
  });
});

describe('社長承認', () => {
  it('100万円超の決裁は社長承認', () => {
    const requisition = new Requisition([
      { price: 300000 },
      { price: 300000 },
      { price: 400001 },
    ], [主任, 課長, 部長]);
    expect(() => requisition.checkRoute()).toThrow("この申請は社長の最終承認が必要です");;
  });
});
