import { CircuitBreaker, ClosedState, HalfOpenState, OpenState, Result } from "./circuit_breaker.after";

describe('new CurcuitBreaker', () => {
    const circuitBreaker = new CircuitBreaker();
    it('closed->closed', () => {
        const current = new Date().getTime();
        const state = new ClosedState([current, current + 1]);
        state.threshold = 4;
        expect(state.nextState(Result.fail(new Error()))).toBeInstanceOf(ClosedState);
    });
    it('closed->open', () => {
        const current = new Date().getTime();
        const state = new ClosedState([current, current + 1]);
        state.threshold = 3;
        expect(state.nextState(Result.fail(new Error()))).toBeInstanceOf(OpenState);
    });
    it('open->open', () => {
        const current = new Date().getTime();
        const state = new OpenState();
        state.opendAt = current;
        state.openToHalfOpenWaitTime = 1000;
        expect(state.nextState()).toBeInstanceOf(OpenState);
    });
    it('open->halfOpen', () => {
        const current = new Date().getTime();
        const state = new OpenState();
        state.opendAt = current - 1010;
        state.openToHalfOpenWaitTime = 1000;
        expect(state.nextState()).toBeInstanceOf(HalfOpenState);
    });
});