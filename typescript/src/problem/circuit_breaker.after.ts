export class Result {
    private success: boolean;
    error: Error;

    static ok(): Result {
        const result = new Result();
        result.success = true;
        return result;
    }

    static fail(err: Error): Result {
        const result = new Result();
        result.success = false;
        return result;
    }

    isOk(): boolean {
        return this.success;
    }
}

function perform(action: () => void): Result {
    try {
        action();
        return Result.ok();
    } catch (e) {
        return Result.fail(e);
    }
}
type CircuitBreakerState = PerformableState | ImperformableState;
abstract class PerformableState {
    abstract nextState(result: Result): CircuitBreakerState;
}
abstract class ImperformableState {
    abstract nextState(): CircuitBreakerState;
}
export class CircuitBreaker {
    state: CircuitBreakerState;
    constructor() {
        this.state = new ClosedState();
    }
    executeAction(action: () => void): void {
        if (this.state instanceof PerformableState) {
            const result = perform(action);
            this.state = this.state.nextState(result);
        } else if (this.state instanceof ImperformableState) {
            this.state = this.state.nextState();
        }
    }
}
export class ClosedState extends PerformableState {
    threshold: number;
    duration: number;
    private errorTimestamps: Array<number>;
    constructor(errorTimestamps: Array<number> | void) {
        super();
        this.threshold = 3;
        this.duration = 1000;
        this.errorTimestamps = errorTimestamps || [];
    }
    nextState(result: Result): CircuitBreakerState {
        if (result.isOk()) {
            return this;
        } else {
            const current: number = new Date().getTime();
            this.errorTimestamps.push(current);
            const timestamps = this.errorTimestamps
                .filter(ts => ts > current - this.duration);
            if (timestamps.length >= this.threshold) {
                return new OpenState();
            } else {
                return new ClosedState(timestamps);
            }
        }
    }
}
export class OpenState extends ImperformableState {
    opendAt: number;
    openToHalfOpenWaitTime: number;
    constructor() {
        super();
        this.opendAt = new Date().getTime();
        this.openToHalfOpenWaitTime = 3000;
    }
    nextState(): CircuitBreakerState {
        if (new Date().getTime() - this.opendAt > this.openToHalfOpenWaitTime) {
            return new HalfOpenState();
        } else {
            return this;
        }
    }
}
export class HalfOpenState extends PerformableState {
    threshold: number;
    successCount: number
    constructor(successCount: number | void) {
        super();
        this.threshold = 3;
        this.successCount = successCount || 0;
    }
    nextState(result: Result) {
        if (result.isOk()) {
            if (this.successCount + 1 >= this.threshold) {
                return new ClosedState();
            } else {
                return new HalfOpenState(this.successCount + 1);
            }
        } else {
            return new OpenState();
        }
    }
}