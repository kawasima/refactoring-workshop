import { Contact } from "./primitive_obsession.before";

describe('Before', () => {
    it('Invalid Name', () => {
        const contact = new Contact();
        expect(() => contact.validate()).toThrow(/Invalid Name/);
    });
    it('Valid contact', () => {
        const contact = new Contact();
        contact.firstName = 'John';
        contact.lastName = 'Doe';
        expect(() => contact.validate()).not.toThrow();
    })
    it('Invalid Address', () => {
        const contact = new Contact();
        contact.firstName = 'John';
        contact.lastName = 'Doe';
        contact.street = '123 Main St';
        expect(() => contact.validate()).toThrow(/Invalid Address/);
    })

    it('send email', () => {
        const contact = new Contact();
        contact.firstName = 'John';
        contact.lastName = 'Doe';
        contact.street = '123 Main St';
        contact.emailAddress = 'abc@example.com';
        contact.isEmailVerified = true;
        expect(contact.sendMessage('Hello')).toContain(contact.emailAddress);
    })
    it('send postal', () => {
        const contact = new Contact();
        contact.firstName = 'John';
        contact.lastName = 'Doe';
        contact.street = '123 Main St';
        contact.postalCode = '12345';
        contact.prefectureCd = 'NY';
        contact.city = 'New York';
        contact.building = 'Empire State Building';
        expect(contact.sendMessage('Hello')).toContain(contact.postalCode);
    })
    it('send tel', () => {
        const contact = new Contact();
        contact.firstName = 'John';
        contact.lastName = 'Doe';
        contact.street = '123 Main St';
        contact.telNo = '123-456-7890';
        expect(contact.sendMessage('Hello')).toContain(contact.telNo);
    })
});
