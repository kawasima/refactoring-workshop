
export class Name {
    firstName: string;
    middleInitial?: string;
    lastName: string;

    constructor(firstName: string, lastName: string, middleInitial?: string) {
        if (firstName === undefined || lastName === undefined) throw new Error('Invalid Name');
        this.firstName = firstName;
        this.middleInitial = middleInitial;
        this.lastName = lastName;
    }
}

export class Email {
    emailAddress: string;
    isEmailVerified: boolean;

    constructor(emailAddress: string, isEmailVerified?: boolean) {
        if (emailAddress === undefined) throw new Error('Invalid Email');
        this.emailAddress = emailAddress;
        this.isEmailVerified = isEmailVerified || false;
    }

    sendMessage(message: string): string {
        if (this.isEmailVerified) {
            return `send ${message} to ${this.emailAddress}`;
        }
    }
}

export class PostalAddress {
    postalCode: string;
    prefectureCd: string;
    city: string;
    street: string;
    building?: string;

    constructor(postalCode: string, prefectureCd: string, city: string, street: string, building?: string) {
        if (postalCode === undefined || prefectureCd === undefined || city === undefined || street === undefined) {
            throw new Error('Invalid Address');
        }
        this.postalCode = postalCode;
        this.prefectureCd = prefectureCd;
        this.city = city;
        this.street = street;
        this.building = building;
    }

    sendMessage(message: string): string {
        return `send ${message} to ${this.postalCode} ${this.prefectureCd} ${this.city} ${this.street} ${this.building}`;
    }
}

export class Tel {
    telNo: string;

    constructor(telNo: string) {
        this.telNo = telNo;
    }

    sendMessage(message: string): string {
        return `send ${message} to ${this.telNo}`;
    }

}

export class Contact {
    name: Name;
    email?: Email;
    tel?: Tel;
    postalAddress?: PostalAddress;

    constructor(name: Name, args?: { email?: Email, postalAddress?: PostalAddress, tel?: Tel }) {
        this.name = name;
        this.email = args?.email;
        this.tel = args?.tel;
        this.postalAddress = args?.postalAddress;
    }

    sendMessage(message: string): string {
        if (this.email) {
            return this.email.sendMessage(message);
        } else if (this.postalAddress) {
            return this.postalAddress.sendMessage(message);
        } else if (this.tel) {
            return this.tel.sendMessage(message);
        } else {
            throw new Error('Cannot contact');
        }
    }
}
