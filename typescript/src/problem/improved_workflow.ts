// Nominal typing, see https://speakerdeck.com/okunokentaro/harajukuts2
type Nominal<T, U extends String> = T & { __brand: U };
export type EmployeeId = Nominal<string, 'EmployeeId'>;
class Position {
  name: string;
  private rank: number;
  constructor(name: string, rank: number) {
    this.name = name;
    this.rank = rank;
  }
  greaterEqualThan(other: Position): boolean {
    return this.rank >= other.rank;
  }
  equals(other: Position): boolean {
    return this.name === other.name;
  }
  toString(): string {
    return JSON.stringify(this);
  }
}

const 主任 = new Position('主任', 10);
const 課長 = new Position('課長', 20);
const 部長 = new Position('部長', 40);
const 社長 = new Position('社長', 80);

export const Positions = { 主任, 課長, 部長, 社長 };

export class Employee {
  id: EmployeeId;
  position: Position;

  constructor(id: EmployeeId, position: Position) {
    this.id = id;
    this.position = position;
  }

  equals(other: Employee) {
    if (other == null) return null;
    return other.id === this.id;
  }

  toString(): string {
    return JSON.stringify(this);
  }
}

export class Price {
  amount: number;
  constructor(amount: number) {
    if (!Number.isInteger(amount)) {
      throw `price must be integer`;
    }
    if (amount < 0) {
      throw `price must be positive`;
    }
    this.amount = amount;
  }

  add(p: Price): Price {
    return new Price(this.amount + p.amount);
  }

  lessThanEqual(p: Price): boolean {
    return this.amount <= p.amount
  }
  asNumber(): number {
    return this.amount;
  }
}

type Approver = Employee

/**
 * 承認ルート
 */
export class ApproveRoute {
  private approvers: Approver[];

  constructor(employees: Employee[]) {
    this.approvers = employees;
  }

  finalApprover(): Approver {
    return this.approvers.slice(-1)[0];
  }

  isFinalApprover(approver: Approver): boolean {
    return this.finalApprover().equals(approver);
  }

  nextApprover(prevApprover: Approver): Approver | null {
    if (!prevApprover) return this.approvers[0];
    const idx = this.approvers.findIndex(a => a.equals(prevApprover));
    if (idx < 0) return null;
    if (idx === this.approvers.length -1) return null;
    return this.approvers[idx+1];
  }
  every(callback: (approver: Approver, index: number, approvers: Approver[]) => boolean): boolean {
    return this.approvers.every(callback);
  }
}

abstract class RequisitionRule {
  abstract validate(requisition: Requisition): void;
}

class AmountBaseRule extends RequisitionRule {
  validate(requisition: Requisition) : void | Error {
    const total = requisition.requisitionLines.totalPrice;
    const finalApprover = requisition.approveRoute.finalApprover();

    if (total.lessThanEqual(new Price(200000))) {
      // 課長承認
      if (!finalApprover.position.equals(課長)) {
        return new Error(`この申請は課長の最終承認が必要です`);
      }
    } else if (total.lessThanEqual(new Price(1000000))) {
      // 部長承認
      if (!finalApprover.position.equals(部長)) {
        return new Error(`この申請は部長の最終承認が必要です`);
      }
    } else {
      // 社長承認
      if (!finalApprover.position.equals(社長)) {
        return new Error(`この申請は社長の最終承認が必要です`);
      }
    }
  }
}

class PositionOrderRule extends RequisitionRule {
  validate(requisition: Requisition): void | Error {
    const isValid = requisition.approveRoute.every((approver, idx, approvers) => {
      if (idx > 0) {
        approver.position.greaterEqualThan(approvers[idx - 1].position);
      }
      return true;
    });
    if (!isValid) {
      return new Error(`承認者順は役職の低い方から高い方へと並んでなくはなりません`);
    }
  }
}

export class RequisitionLine {
  price: Price;
  constructor(price: Price) {
    this.price = price;
  }
}

export class RequisitionLines {
  lines: RequisitionLine[];
  constructor(requisitionLines: [RequisitionLine, ...RequisitionLine[]]) {
    this.lines = requisitionLines;
  }

  get totalPrice(): Price {
    return this.lines.reduce((prev, cur) => prev.add(cur.price), new Price(0));
  }
}

type ApprovedEvent = {
  approvedAt: Date;
  approver: Employee;
}

type DeclinedEvent = {
  declinedAt: Date;
  decliner: Employee;
}

type WorkflowEvent = ApprovedEvent | DeclinedEvent;

class WorkflowHistory {
  
}

interface Appliable {
  apply(): AppliedState;
}
interface Declinable {
  decline(): DeclinedState;
}
interface Approvable {
  approve(approver: Approver): AppliedState;
  finalApprove(approver: Approver): FinalApprovedState;
}

export class AppliedState implements Approvable, Declinable {
  lastApprover: Approver;

  constructor(approver?: Approver) {
    this.lastApprover = approver;
  }

  approve(approver: Approver): AppliedState {
    return new AppliedState(approver);
  }
  finalApprove(approver: Approver): FinalApprovedState {
    return new FinalApprovedState();
  }

  decline(): DeclinedState {
    return new DeclinedState();
  }
}

export class DeclinedState implements Appliable {
  apply(): AppliedState {
    return new AppliedState();
  }
}
export class FinalApprovedState {
}

export class DraftState implements Appliable {
  apply(): AppliedState {
    return new AppliedState();
  }
}

type RequisitionStatus = DraftState | AppliedState | DeclinedState | FinalApprovedState;
/**
 * 購買申請
 */
export class Requisition {
  /** 承認ステータス */
  requisitionState: RequisitionStatus;
  /** 承認ルート */
  approveRoute: ApproveRoute;
  /** 購入商品 */
  requisitionLines: RequisitionLines;
  /** 承認履歴 */
  workflowHistory: WorkflowEvent[];

  static requisitionRules: RequisitionRule[] = [new AmountBaseRule(), new PositionOrderRule()];

  constructor(requisitionLines: RequisitionLines,
    approveRoute: ApproveRoute) {
    this.requisitionLines = requisitionLines;
    this.approveRoute = approveRoute;
    this.workflowHistory = [];
    this.requisitionState = new DraftState();
    Requisition.requisitionRules.every(rule => {
      const err = rule.validate(this);
      if (err != null) throw err;
    });
  }

  applied(): void | Error {
    // ほんとは this.requitisionState instanceof Appliable としたい
    if (this.requisitionState instanceof DraftState || this.requisitionState instanceof DeclinedState) {
      this.requisitionState = this.requisitionState.apply();
    } else {
      return new Error(`ワークフローは申請できない状態です`);
    }
  }

  approve(approver: Approver): void | Error {
    if (this.requisitionState instanceof AppliedState) {
      const nextApprover = this.approveRoute.nextApprover(this.requisitionState.lastApprover);
      if (!approver.equals(nextApprover)) {
        throw new Error(`承認ルートと合致しない承認者です ${approver}`);
      }
      this.requisitionState = this.approveRoute.isFinalApprover(nextApprover) ?
        this.requisitionState.finalApprove(nextApprover)
        :
        this.requisitionState.approve(nextApprover)
      this.workflowHistory.push({ approver, approvedAt: new Date()});
    } else {
      return new Error(`ワークフローは承認できない状態です`);
    }
  }

  decline(decliner: Approver): void | Error {
    if (this.requisitionState instanceof AppliedState) {
      const nextApprover = this.approveRoute.nextApprover(this.requisitionState.lastApprover);
      if (!decliner.equals(nextApprover)) {
        throw new Error(`承認ルートと合致しない承認者です ${decliner}`);
      } 
      this.requisitionState = this.requisitionState.decline();
      this.workflowHistory.push({ decliner, declinedAt: new Date()});
    } else {
      return new Error(`ワークフローは否認できない状態です`);
    }
  }
}