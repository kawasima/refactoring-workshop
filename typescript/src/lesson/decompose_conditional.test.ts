import { ComparableDate, Plan, Before, After } from "./decompose_conditional";

const plan: Plan = {
    summerStart: new ComparableDate(2022, 6, 1),
    summerEnd: new ComparableDate(2022, 8, 31),
    summerRate: 1.08,
    regularRate: 1.1,
    regularServiceCharge: 100
};

describe("test before", () => {
    it("summer rate", () => {
        expect(Before.charge(10, new ComparableDate(2022, 7, 1), plan)).toBe(10.8);
    });
    it("regular rate", () => {
        expect(Before.charge(10, new ComparableDate(2022, 9, 1), plan)).toBe(111);
    });
});

describe("test after", () => {
    it("is summer", () => {
        expect(After.summer(new ComparableDate(2022, 6, 1), plan)).toBe(true);
        expect(After.summer(new ComparableDate(2022, 8, 31), plan)).toBe(true);
    });
    it("is not summer", () => {
        expect(After.summer(new ComparableDate(2022, 5, 31), plan)).toBe(false);
        expect(After.summer(new ComparableDate(2022, 9, 1), plan)).toBe(false);
    });

    it("summer rate", () => {
        expect(After.charge(10, new ComparableDate(2022, 7, 1), plan)).toBe(10.8);
    });
    it("regular rate", () => {
        expect(After.charge(10, new ComparableDate(2022, 9, 1), plan)).toBe(111);
    });
});