
export class ComparableDate {
    private value: Date;
    constructor(year: number, month: number, day: number) {
        this.value = new Date(year, month-1, day);
        // this.value = new Date(year, month, day);
    }
    isBefore(other: ComparableDate): boolean {
        return this.value.getTime() < other.value.getTime();
    }
    isAfter(other: ComparableDate): boolean {
        return this.value.getTime() > other.value.getTime();
    }
    asDate(): Date {
        return this.value;
    }
}

export type Plan = {
    summerStart: ComparableDate;
    summerEnd: ComparableDate;
    summerRate: number;
    regularRate: number;
    regularServiceCharge: number;
}

export namespace Before {
    export function charge(quantity: number, aDate: ComparableDate, plan: Plan) : number {
        if (!aDate.isBefore(plan.summerStart) && !aDate.isAfter(plan.summerEnd))
            return quantity * plan.summerRate;
        else
            return quantity * plan.regularRate + plan.regularServiceCharge;
    }
}

export namespace After {
    export function summer(aDate: ComparableDate, plan: Plan): boolean {
      return !aDate.isBefore(plan.summerStart) && !aDate.isAfter(plan.summerEnd);
    }
    export function summerCharge(quantity: number, plan: Plan): number {
      return quantity * plan.summerRate;
    }
    export function regularCharge(quantity: number, plan: Plan): number {
      return quantity * plan.regularRate + plan.regularServiceCharge;
    }    
    export function charge(quantity: number, aDate: ComparableDate, plan: Plan) : number {
        return summer(aDate, plan) ? summerCharge(quantity, plan) : regularCharge(quantity, plan);
    }
}