import { Offer, DRAFT, PUBLISHED } from "./primitive_obsession";

describe('test', () => {
    it('offer new', () => {
        const offer = new Offer();
        expect(offer.getStatus()).toBe(DRAFT);
    });

    it('status set', () => {
        const offer = new Offer();
        offer.setStatus(PUBLISHED);
        expect(offer.getStatus()).toBe(PUBLISHED);
    });
});
