export namespace Before {
    type Bird = {
        type: string;
        name: string;
        numberOfCocounts: number;
        voltage: number;
        isNailed: boolean;
    }
    function plumanges(birds: Array<Bird>) {
        return new Map(birds.map(b => [b.name, plumage(b)]));
    }
    function plumage(bird: Bird) {
        switch(bird.type) {
        case 'EuropeanSwallow':
            return "average";
        case 'AfricanSwallow':
            return (bird.numberOfCocounts > 2) ? "tired" : "average";
        case 'NorwegianBlueParrot':
            return (bird.voltage > 100) ? "scorched" : "beautiful";
        default:
            return "unknown";
        }
    }
    function airSpeedVelocity(bird: Bird) {
        switch(bird.type) {
        case 'EuropianSwallow':
            return 35;
        case 'AfricanSwallow':
            return 40 - 2 * bird.numberOfCocounts;
        case 'NorwegianBlueParrot':
            return (bird.isNailed) ? 0 : 10 + bird.voltage / 10;
        default:
            return null;
        }
    }
}

export namespace After {
    export class Bird {
        type: string;
        name: string;
        numberOfCocounts: number;
        voltage: number;
        isNailed: boolean;

        constructor({ type, name, numberOfCocounts, voltage, isNailed }: {
            type: string,
            name: string,
            numberOfCocounts: number;
            voltage: number;
            isNailed: boolean;
        }) {
            Object.assign(this, arguments);
        }
        get plumage(): string {
            return "unknown";
        }
        get airSpeedVelocity(): null | number {
            return null;
        }
    }
    class EuropeanSwallow extends Bird {
        get plumage() {
            return "average";
        }
        get airSpeedVelocity(): number {
            return 35;
        }
    }
    class AfricanSwallow extends Bird {
        get plumage() {
            return (this.numberOfCocounts > 2) ? "tired" : "average";
        }
        get airSpeedVelocity(): number {
            return 40 - 2 * this.numberOfCocounts;
        }
    }
    class NorwegianBlueParrot extends Bird {
        get plumage(): string {
            return (this.voltage > 100) ? "scorched" : "beautiful";
        }
        get airSpeedVelocity(): number {
            return (this.isNailed) ? 0 : 10 + this.voltage / 10;
        }
    }
}