export const DRAFT: number = 1;
export const PUBLISHED: number = 2;
export const CANCELED: number = 3;

export class Offer {
  private status: number;

  constructor() {
    this.status = DRAFT;
  }

  setStatus(status: number): void {
    this.status = status;
  }
  getStatus(): number {
    return this.status;
  }
}
