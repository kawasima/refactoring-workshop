import { Before, After } from "./replace_primitive_with_object";

describe("Test before", () => {
    it("high priority count", () => {
        const orders = [
            new Before.Order({priority: "high"}),
            new Before.Order({priority: "normal"}),
            new Before.Order({priority: "urgent"}),
            new Before.Order({priority: "low"}),
        ];
        expect(Before.highPriorityCount(orders)).toBe(1);
    });
});

describe("Test after", () => {
    it("high priority count", () => {
        const orders = [
            new After.Order({ priority: After.Priority.HIGH }),
            new After.Order({ priority: After.Priority.LOW }),
            new After.Order({ priority: After.Priority.NORMAL }),
            new After.Order({ priority: After.Priority.RUSH }),
        ];
        expect(After.highPriorityCount(orders)).toBe(2);
    });
});