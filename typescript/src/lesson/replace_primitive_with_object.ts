export namespace Before {
    export class Order {
        priority: string;
        constructor({ priority }: { priority: string }) {
            this.priority = priority;
        }
    }

    export function highPriorityCount(orders: Array<Order>): number {
        return orders
            .filter(o => "high" == o.priority
              || "rush" == o.priority)
            .length;
    }
}

export namespace After {
    export class Priority {
        private value: string;
        constructor(value: Priority | string) {
            if (value instanceof Priority) return value;
            if (Priority.legalValues().includes(value)) {
                this.value = value;
            }
        }
        asString() : string {
            return this.value;
        }
        private get index() : number {
            return Priority.legalValues().findIndex(s => s === this.value);
        }
        equals(other: Priority): boolean {
            return this.index === other.index;
        }
        higherThan(other: Priority) {
            return this.index > other.index;
        }
        lowerThan(other: Priority) {
            return this.index < other.index;
        }
        static legalValues() {
            return ["low", "normal", "high", "rush"];
        }
        static LOW = new Priority("low");
        static NORMAL = new Priority("normal");
        static HIGH = new Priority("high");
        static RUSH = new Priority("rush");
    }

    export class Order {
        priority: Priority;
        constructor({ priority }: { priority: Priority }) {
            this.priority = priority;
        }
    }

    export function highPriorityCount(orders: Array<Order>): number {
        return orders
            .filter(o => o.priority.higherThan(Priority.NORMAL))
            .length;
    }
}

