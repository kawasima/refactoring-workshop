package lesson.refusedbequest.after;

public class FreeAnswer implements Answer {
    private final FreeQuestion question;
    private final String description;

    public FreeAnswer(FreeQuestion question, String description) {
        this.question = question;
        this.description = description;
    }

    @Override
    public Question getQuestion() {
        return question;
    }

    public String getDescription() {
        return description;
    }
}
