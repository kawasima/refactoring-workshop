package lesson.refusedbequest.after;

public interface Answer {
    Question getQuestion();
}
