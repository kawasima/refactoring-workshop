package lesson.refusedbequest.after;

import java.util.List;
import java.util.stream.Collectors;

public class MultipleChoiceQuestion implements Question {
    private String lead;
    private List<String> options;

    @Override
    public String render() {
        return "<p>" + lead + "</p>" +
                "<ul>" +
                options.stream()
                        .map(option -> "<li>" + option + "</li>")
                        .collect(Collectors.joining()) +
                "</ul>";
    }

    public String getLead() {
        return lead;
    }

    public List<String> getOptions() {
        return options;
    }
}
