package lesson.refusedbequest.after;

public class FreeQuestion implements Question {
    private final String lead;

    public FreeQuestion(String lead) {
        this.lead = lead;
    }

    @Override
    public String render() {
        return "<p>" + lead + "</p>";
    }
}
