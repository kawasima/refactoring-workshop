package lesson.refusedbequest.after;

public class MultipleChoiceAnswer implements Answer {
    private final MultipleChoiceQuestion question;
    private final int choice;

    public MultipleChoiceAnswer(MultipleChoiceQuestion question, int choice) {
        this.question = question;
        this.choice = choice;
    }
    @Override
    public Question getQuestion() {
        return question;
    }

    public int getChoice() {
        return choice;
    }
}
