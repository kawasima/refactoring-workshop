package lesson.refusedbequest.before;

public class FreeQuestion extends Question {
    @Override
    public String render() {
        return "<p>" + getLead() + "</p>";
    }
}
