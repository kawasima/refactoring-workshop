package lesson.refusedbequest.before;

public class FreeAnswer extends Answer {
    private final String description;
    public FreeAnswer(FreeQuestion question, String description) {
        super(question, 0);
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
