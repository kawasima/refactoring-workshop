package lesson.refusedbequest.before;

import java.util.List;
import java.util.stream.Collectors;

public class Question {
    private String lead;
    private List<String> options;

    public String render() {
        return "<p>" + lead + "</p>" +
                "<ul>" +
                options.stream()
                        .map(option -> "<li>" + option + "</li>")
                        .collect(Collectors.joining()) +
                "</ul>";
    }

    public String getLead() {
        return lead;
    }

    public List<String> getOptions() {
        return options;
    }
}
