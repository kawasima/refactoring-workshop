package lesson.refusedbequest.before;

public class Answer {
    private final Question question;
    private final int choice;

    public Answer(Question question, int choice) {
        this.question = question;
        this.choice = choice;
    }

    public Question getQuestion() {
        return question;
    }

    public int getChoice() {
        return choice;
    }
}
