package lesson.replaceprimitives.before;

public class Order {
    private final String priority;
    public Order(String priority) {
        this.priority = priority;
    }

    public String getPriority() {
        return priority;
    }
}
