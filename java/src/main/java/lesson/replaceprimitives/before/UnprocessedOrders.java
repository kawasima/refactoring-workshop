package lesson.replaceprimitives.before;

import java.util.List;
import java.util.Objects;

public class UnprocessedOrders {
    private final List<Order> orders;

    public UnprocessedOrders(List<Order> orders) {
        this.orders = orders;
    }

    public long highPriorityCount() {
        return orders.stream()
                .filter(o -> Objects.equals(o.getPriority(), "high")
                        || Objects.equals(o.getPriority(), "rush"))
                .count();
    }
}
