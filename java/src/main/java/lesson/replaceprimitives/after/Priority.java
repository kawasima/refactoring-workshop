package lesson.replaceprimitives.after;

public enum Priority {
    LOW(10),
    NORMAL(20),
    HIGH(30),
    RUSH(40);

    Priority(int value) {
        this.value = value;
    }
    private final int value;

    public boolean higherThan(Priority another) {
        return this.value > another.value;
    }

    public boolean lowerThan(Priority another) {
        return this.value < another.value;
    }
}
