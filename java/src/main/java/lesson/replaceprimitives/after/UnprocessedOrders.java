package lesson.replaceprimitives.after;

import java.util.List;

public class UnprocessedOrders {
    private final List<Order> orders;

    public UnprocessedOrders(List<Order> orders) {
        this.orders = orders;
    }

    public long highPriorityCount() {
        return orders.stream()
                .filter(o -> o.getPriority().higherThan(Priority.NORMAL))
                .count();
    }
}
