package lesson.dataclumps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SendMessageService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SendMessageService.class);
    void sendEmail(String emailAddress) {
        LOGGER.info("Send an email to {}", emailAddress);
    }

    void sendDM(String zip, String state, String city, String address1, String address2) {
        LOGGER.info("Send a DM to {} {} {} {} {}", zip, state, city, address1, address2);
    }

    public void send(Contact contact) {
        if (contact.getZip() != null && contact.getState() != null) {
            sendDM(contact.getZip(), contact.getState(), contact.getCity(), contact.getAddress1(), contact.getAddress2());
        } else if (contact.getEmailAddress() != null && contact.isEmailVerified()) {
            sendEmail(contact.getEmailAddress());
        }
    }
}
