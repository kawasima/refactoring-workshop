package lesson.decomposeconditional;

import java.math.BigDecimal;

public class Plan {
    private final ComparableDate summerStart;
    private final ComparableDate summerEnd;
    private final BigDecimal summerRate;
    private final BigDecimal regularRate;
    private final BigDecimal regularServiceCharge;

    public Plan(ComparableDate summerStart, ComparableDate summerEnd, BigDecimal summerRate, BigDecimal regularRate, BigDecimal regularServiceCharge) {
        this.summerStart = summerStart;
        this.summerEnd = summerEnd;
        this.summerRate = summerRate;
        this.regularRate = regularRate;
        this.regularServiceCharge = regularServiceCharge;
    }

    public ComparableDate getSummerStart() {
        return summerStart;
    }

    public ComparableDate getSummerEnd() {
        return summerEnd;
    }

    public BigDecimal getSummerRate() {
        return summerRate;
    }

    public BigDecimal getRegularRate() {
        return regularRate;
    }

    public BigDecimal getRegularServiceCharge() {
        return regularServiceCharge;
    }
}
