package lesson.decomposeconditional;

import java.util.Calendar;
import java.util.Date;

public class ComparableDate {
    private final Date value;

    public ComparableDate(int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, month, day);
        value = cal.getTime();
    }

    public boolean isBefore(ComparableDate other) {
        return value.getTime() < other.value.getTime();
    }

    public boolean isAfter(ComparableDate other) {
        return value.getTime() > other.value.getTime();
    }

    public Date asDate() {
        return value;
    }
}
