package lesson.decomposeconditional;

import java.math.BigDecimal;

public class ChargerBefore {
    public static BigDecimal charge(int quantity, ComparableDate aDate, Plan plan) {
        if (!aDate.isBefore(plan.getSummerStart()) && !aDate.isAfter(plan.getSummerEnd())) {
            return plan.getSummerRate().multiply(new BigDecimal(quantity));
        } else {
            return plan.getRegularRate().multiply(new BigDecimal(quantity))
                    .add(plan.getRegularServiceCharge());
        }
    }
}
