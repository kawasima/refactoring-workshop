package lesson.decomposeconditional;

import java.math.BigDecimal;

public class ChargerAfter {
    public static boolean summer(ComparableDate aDate, Plan plan) {
        return !aDate.isBefore(plan.getSummerStart()) && !aDate.isAfter(plan.getSummerEnd());
    }

    public static BigDecimal summerCharge(int quantity, Plan plan) {
        return plan.getSummerRate().multiply(new BigDecimal(quantity));
    }

    public static BigDecimal regularCharge(int quantity, Plan plan) {
        return plan.getRegularRate().multiply(new BigDecimal(quantity))
                .add(plan.getRegularServiceCharge());
    }

    public static BigDecimal charge(int quantity, ComparableDate aDate, Plan plan) {
        return summer(aDate, plan) ?
                summerCharge(quantity, plan)
                :
                regularCharge(quantity, plan);
    }
}
