package lesson.featureenvy;

import java.math.BigDecimal;

public class CartPricer {
    private final TaxManager taxManager;

    public CartPricer(TaxManager taxManager) {
        this.taxManager = taxManager;
    }

    public BigDecimal total(Cart cart) {
        return cart.getItems().stream()
                .map(item -> taxManager.getRate(cart.getUser().getCountry())
                        .multiply(new BigDecimal(item.getSku().getPrice()))
                        .multiply(new BigDecimal(item.getAmount())))
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);
    }
}
