package lesson.featureenvy;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class TaxManager {
    private final Map<String, BigDecimal> taxByCountry;

    public TaxManager() {
        taxByCountry = new HashMap<>();
    }

    public void add(String country, BigDecimal rate) {
        taxByCountry.put(country, rate);
    }

    public BigDecimal getRate(String country) {
        return taxByCountry.get(country);
    }
}
