package lesson.featureenvy;

public class CartItem {
    private final SKU sku;
    private final int amount;

    public CartItem(SKU sku, int amount) {
        this.sku = sku;
        this.amount = amount;
    }

    public SKU getSku() {
        return sku;
    }

    public int getAmount() {
        return amount;
    }
}
