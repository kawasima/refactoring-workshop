package lesson.featureenvy;

public class SKU {
    private final String skuId;
    private final String name;
    private final long price;

    public SKU(String skuId, String name, long price) {
        this.skuId = skuId;
        this.name = name;
        this.price = price;
    }

    public String getSkuId() {
        return skuId;
    }

    public String getName() {
        return name;
    }

    public long getPrice() {
        return price;
    }
}
