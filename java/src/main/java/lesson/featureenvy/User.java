package lesson.featureenvy;

public class User {
    private final String userId;
    private final String country;

    public User(String userId, String country) {
        this.userId = userId;
        this.country = country;
    }

    public String getUserId() {
        return userId;
    }

    public String getCountry() {
        return country;
    }
}
