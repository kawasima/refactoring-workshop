package lesson.featureenvy;

import java.util.ArrayList;
import java.util.List;

public class Cart {
    private final User user;
    private final List<CartItem> items;

    public Cart(User user) {
        this.user = user;
        items = new ArrayList<>();
    }

    public void add(SKU sku, int amount) {
        items.add(new CartItem(sku, amount));
    }

    public User getUser() {
        return user;
    }

    public List<CartItem> getItems() {
        return items;
    }
}
