package lesson.polymorphism;

import java.io.*;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class IO {
    public void copy() {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void copy2(Supplier<String> reader, Consumer<String> writer) {
        String line;
        while ((line = reader.get()) != null) {
            writer.accept(line);
        }
    }

    public void test() {
        copy2(() -> {
            try {
                return new BufferedReader(new InputStreamReader(System.in)).readLine();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }, System.out::println);
    }

    public void copy3(String type) {
        switch(type) {
            case "console":
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
                    String line;
                    while ((line = reader.readLine()) != null) {
                        System.out.println(line);
                    }
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                break;
            case "file":
                try (BufferedReader reader = new BufferedReader(new FileReader("in.txt"));
                     BufferedWriter writer = new BufferedWriter(new FileWriter("out.txt"))) {
                    String line;
                    while ((line = reader.readLine()) != null) {
                        writer.write(line);
                        writer.newLine();
                    }
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown type: " + type);
        }
    }
}
