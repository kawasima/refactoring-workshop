package lesson.polymorphism;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Inheritance {
    public abstract class CopierByLine {
        public abstract String read();
        public abstract void write(String line);
        public void copy() {
            String line;
            while ((line = read()) != null) {
                write(line);
            }
        }
    }

    public class ConsoleCopier extends CopierByLine {
        private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        @Override
        public String read() {
            try {
                return reader.readLine();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public void write(String line) {
            System.out.println(line);
        }
    }
}
