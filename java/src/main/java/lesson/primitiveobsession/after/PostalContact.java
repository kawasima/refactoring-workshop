package lesson.primitiveobsession.after;

public class PostalContact {
    private final PostalAddress address;
    private boolean isAddressValid;

    public PostalContact(PostalAddress address) {
        this.address = address;
        isAddressValid = false;
    }

    public void validate() {
        isAddressValid = true;
    }

    public PostalAddress getAddress() {
        return address;
    }

    public boolean isAddressValid() {
        return isAddressValid;
    }
}
