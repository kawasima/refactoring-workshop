package lesson.primitiveobsession.after;

public enum CustomerRank {
    GOLD,
    SILVER,
    BRONZE,
}
