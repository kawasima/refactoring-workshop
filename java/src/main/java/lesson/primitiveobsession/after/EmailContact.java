package lesson.primitiveobsession.after;

public class EmailContact {
    private final String emailAddress;
    private boolean isEmailVerified;

    public EmailContact(String emailAddress) {
        this.emailAddress = emailAddress;
        this.isEmailVerified = false;
    }

    public void verify() {
        isEmailVerified = true;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public boolean isEmailVerified() {
        return isEmailVerified;
    }
}
