package lesson.primitiveobsession.after;

public class Contact {
    private PersonalName name;
    private EmailContact emailContact;
    private PostalContact postalContact;
    private CustomerRank customerRank;

    public PersonalName getName() {
        return name;
    }

    public void setName(PersonalName name) {
        this.name = name;
    }

    public EmailContact getEmailContact() {
        return emailContact;
    }

    public void setEmailContact(EmailContact emailContact) {
        this.emailContact = emailContact;
    }

    public PostalContact getPostalContact() {
        return postalContact;
    }

    public void setPostalContact(PostalContact postalContact) {
        this.postalContact = postalContact;
    }

    public CustomerRank getCustomerRank() {
        return customerRank;
    }

    public void setCustomerRank(CustomerRank customerRank) {
        this.customerRank = customerRank;
    }
}
