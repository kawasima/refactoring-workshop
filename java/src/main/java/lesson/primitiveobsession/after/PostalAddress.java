package lesson.primitiveobsession.after;

public class PostalAddress {
    private final String zip;
    private final String state;
    private final String city;
    private final String address1;
    private final String address2;

    public PostalAddress(String zip, String state, String city, String address1, String address2) {
        this.zip = zip;
        this.state = state;
        this.city = city;
        this.address1 = address1;
        this.address2 = address2;
    }

    public String getZip() {
        return zip;
    }

    public String getState() {
        return state;
    }

    public String getCity() {
        return city;
    }

    public String getAddress1() {
        return address1;
    }

    public String getAddress2() {
        return address2;
    }
}
