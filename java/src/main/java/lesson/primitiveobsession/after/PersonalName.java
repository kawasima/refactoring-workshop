package lesson.primitiveobsession.after;

import java.util.Objects;
import java.util.Optional;

public class PersonalName {
    private final String firstName;
    private final String middleInitial;
    private final String lastName;

    public PersonalName(String firstName, String lastName) {
        this(firstName, null, lastName);
    }

    public PersonalName(String firstName, String middleInitial, String lastName) {
        this.firstName = Objects.requireNonNull(firstName);
        this.middleInitial = middleInitial;
        this.lastName = Objects.requireNonNull(lastName);
    }

    public String getFirstName() {
        return firstName;
    }

    public Optional<String> getMiddleInitial() {
        return Optional.ofNullable(middleInitial);
    }

    public String getLastName() {
        return lastName;
    }
}
