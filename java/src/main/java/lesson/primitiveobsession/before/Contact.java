package lesson.primitiveobsession.before;

/**
 * Primitive Obsessionの例
 */
public class Contact {
    public static final int RANK_GOLD = 30;
    public static final int RANK_SILVER = 20;
    public static final int RANK_BRONZE = 20;

    private String firstName;
    private String middleInitial;
    private String lastName;
    private String emailAddress;
    private boolean isEmailVerified;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String zip;
    private boolean isAddressValid;
    private int customerRank;
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleInitial() {
        return middleInitial;
    }

    public void setMiddleInitial(String middleInitial) {
        this.middleInitial = middleInitial;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public boolean isEmailVerified() {
        return isEmailVerified;
    }

    public void setEmailVerified(boolean emailVerified) {
        isEmailVerified = emailVerified;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public boolean isAddressValid() {
        return isAddressValid;
    }

    public void setAddressValid(boolean addressValid) {
        isAddressValid = addressValid;
    }

    public int getCustomerRank() {
        return customerRank;
    }

    public void setCustomerRank(int customerRank) {
        this.customerRank = customerRank;
    }

}
