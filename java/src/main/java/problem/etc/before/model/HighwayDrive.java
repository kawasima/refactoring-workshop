package problem.etc.before.model;

import java.time.LocalDateTime;

public class HighwayDrive {
    private LocalDateTime enteredAt;
    private LocalDateTime exitedAt;
    private String vehicleFamily;
    private String routeType;

    private int countPerMonth;

    public HighwayDrive() {
    }

    public LocalDateTime getEnteredAt() {
        return this.enteredAt;
    }

    public LocalDateTime getExitedAt() {
        return this.exitedAt;
    }

    public String getVehicleFamily() {
        return this.vehicleFamily;
    }

    public String getRouteType() {
        return this.routeType;
    }

    public int getCountPerMonth() {
        return this.countPerMonth;
    }

    public void setEnteredAt(LocalDateTime enteredAt) {
        this.enteredAt = enteredAt;
    }

    public void setExitedAt(LocalDateTime exitedAt) {
        this.exitedAt = exitedAt;
    }

    public void setVehicleFamily(String vehicleFamily) {
        this.vehicleFamily = vehicleFamily;
    }

    public void setRouteType(String routeType) {
        this.routeType = routeType;
    }

    public void setCountPerMonth(int countPerMonth) {
        this.countPerMonth = countPerMonth;
    }

    public String toString() {
        return "HighwayDrive(enteredAt=" + this.getEnteredAt() + ", exitedAt=" + this.getExitedAt() + ", vehicleFamily=" + this.getVehicleFamily() + ", routeType=" + this.getRouteType() + ", countPerMonth=" + this.getCountPerMonth() + ")";
    }
}
