package problem.etc.after;

import one.cafebabe.businesscalendar4j.BusinessCalendar;

import java.time.LocalDate;

import static one.cafebabe.businesscalendar4j.BusinessCalendar.CLOSED_ON_SATURDAYS_AND_SUNDAYS;
import static one.cafebabe.businesscalendar4j.BusinessCalendar.JAPAN;

public class HolidayUtils {
    private static final BusinessCalendar calendar = BusinessCalendar.newBuilder()
            .holiday(JAPAN.PUBLIC_HOLIDAYS)
            .holiday(CLOSED_ON_SATURDAYS_AND_SUNDAYS)
            .build();

    public static boolean isHoliday(LocalDate date) {
        return calendar.isHoliday(date);
    }
}
