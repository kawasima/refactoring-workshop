package problem.etc.after.rule;

import problem.etc.after.DiscountRule;
import problem.etc.after.HolidayUtils;
import problem.etc.after.model.HighwayDrive;
import problem.etc.after.model.VehicleFamily;

import java.util.EnumSet;

import static problem.etc.after.model.RouteType.RURAL;
import static problem.etc.after.model.VehicleFamily.*;

public class DiscountOnHoliday implements DiscountRule {
    private static final EnumSet<VehicleFamily> targetVehicleFamilies =
            EnumSet.of(STANDARD, MOTORCYCLE, MINI);

    @Override
    public boolean isApplicable(HighwayDrive drive) {
        return (HolidayUtils.isHoliday(drive.getEnteredAt().toLocalDate())
                || HolidayUtils.isHoliday(drive.getExitedAt().toLocalDate()))
                && targetVehicleFamilies.contains(drive.getVehicleFamily())
                && drive.getRouteType() == RURAL;
    }

    @Override
    public long discountPercentage(HighwayDrive drive) {
        return 30;
    }}
