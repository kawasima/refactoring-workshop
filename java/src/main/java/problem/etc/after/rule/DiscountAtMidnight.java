package problem.etc.after.rule;

import problem.etc.after.DiscountRule;
import problem.etc.after.RulePeriod;
import problem.etc.after.model.HighwayDrive;

public class DiscountAtMidnight implements DiscountRule {
    private final RulePeriod midnight = new RulePeriod(0, 4);
    @Override
    public boolean isApplicable(HighwayDrive drive) {
        return midnight.isIn(drive);
    }

    @Override
    public long discountPercentage(HighwayDrive drive) {
        return 30;
    }
}
