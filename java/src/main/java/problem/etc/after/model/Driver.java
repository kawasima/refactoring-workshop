package problem.etc.after.model;

public class Driver {
    private int countPerMonth;

    public int getCountPerMonth() {
        return this.countPerMonth;
    }

    public void setCountPerMonth(int countPerMonth) {
        this.countPerMonth = countPerMonth;
    }

    public String toString() {
        return "Driver(countPerMonth=" + this.getCountPerMonth() + ")";
    }
}
