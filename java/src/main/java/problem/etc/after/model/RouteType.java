package problem.etc.after.model;

public enum RouteType {
    /** 地方部 */
    RURAL,
    /** 都市部 */
    URBAN,
}
