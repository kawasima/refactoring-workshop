package problem.etc.after.model;

public enum VehicleFamily {
    /** 普通車 */
    STANDARD,
    /** 軽自動車 */
    MINI,
    /** 二輪車 */
    MOTORCYCLE,
    /** その他 */
    OTHER
}
