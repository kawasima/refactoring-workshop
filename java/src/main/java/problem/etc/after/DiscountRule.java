package problem.etc.after;


import problem.etc.after.model.HighwayDrive;

public interface DiscountRule {
    boolean isApplicable(HighwayDrive drive);
    long discountPercentage(HighwayDrive drive);
}
