package problem.etc.after;

import problem.etc.after.model.HighwayDrive;
import problem.etc.after.rule.DiscountAtMidnight;
import problem.etc.after.rule.DiscountInMorningOrEvening;
import problem.etc.after.rule.DiscountOnHoliday;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class DiscountService {
    List<DiscountRule> discountRules;

    public DiscountService() {
        discountRules = Arrays.asList(
                new DiscountInMorningOrEvening(),
                new DiscountOnHoliday(),
                new DiscountAtMidnight()
        );
    }

    public long calc(HighwayDrive drive) {
        return discountRules.stream()
                .filter(rule -> rule.isApplicable(drive))
                .map(rule -> rule.discountPercentage(drive))
                .max(Comparator.naturalOrder())
                .orElse(0L);
    }
}
