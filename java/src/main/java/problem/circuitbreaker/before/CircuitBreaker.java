package problem.circuitbreaker.before;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class CircuitBreaker {
    private String status;
    private final int errorThreshold;
    private final int successThreshold;
    private int errorCounter;
    private int successCounter;
    private final ScheduledExecutorService closeTimer = Executors.newScheduledThreadPool(1);
    private long openedAt;
    private final long openToHalfOpenWaitTime;

    public CircuitBreaker() {
        status = "close";
        errorThreshold = 3;
        errorCounter = 0;
        successThreshold = 3;
        successCounter = 0;
        openToHalfOpenWaitTime = 1000L;
        closeTimer.scheduleWithFixedDelay(() -> {
            this.errorCounter = 0;
        }, 1L, 1L, TimeUnit.SECONDS);
    }

    public void executeAction(Runnable action) {
        if (status.equals("close")) {
            try {
                action.run();
            } catch(Exception e) {
                errorCounter += 1;
                if (errorCounter >= errorThreshold) {
                    status = "open";
                    openedAt = System.currentTimeMillis();
                }
                throw e;
            }
        } else if (status.equals("open")) {
            if (System.currentTimeMillis() - openedAt > openToHalfOpenWaitTime) {
                status = "half_open";
            }
            throw new CircuitBreakerOpenException();
        } else if (status.equals("half_open")) {
            try {
                action.run();
                successCounter += 1;
                if (successCounter >= successThreshold) {
                    status = "close";
                }
            } catch(Exception e) {
                successCounter = 0;
                status = "open";
                openedAt = System.currentTimeMillis();
                throw e;
            }
        }
    }

    public String getStatus() {
        return status;
    }
}
