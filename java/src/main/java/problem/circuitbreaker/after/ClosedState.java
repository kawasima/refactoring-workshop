package problem.circuitbreaker.after;

import java.util.ArrayList;
import java.util.List;

public class ClosedState implements PerformableState {
    private int threshold;
    private final int duration;
    private final List<Long> errorTimestamps;

    public ClosedState() {
        this(new ArrayList<>());
    }

    public ClosedState(List<Long> errorTimestamps) {
        threshold = 3;
        duration = 1000;
        this.errorTimestamps = new ArrayList<>(errorTimestamps);
    }

    @Override
    public CircuitBreakerState nextState(Result result) {
        if (result.isOk()) {
            return this;
        } else {
            long current = System.currentTimeMillis();
            errorTimestamps.add(current);
            errorTimestamps.removeIf(ts -> ts <= current - duration);
            if (errorTimestamps.size() >= this.threshold) {
                return new OpenState();
            } else {
                return this;
            }
        }
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }
}
