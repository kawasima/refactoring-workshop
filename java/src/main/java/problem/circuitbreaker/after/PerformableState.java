package problem.circuitbreaker.after;

public interface PerformableState extends CircuitBreakerState {
    CircuitBreakerState nextState(Result result);
}
