package problem.circuitbreaker.after;

public class HalfOpenState implements PerformableState {
    private final int threshold;
    private final int successCount;

    public HalfOpenState() {
        this(0);
    }

    public HalfOpenState(int successCount) {
        threshold = 3;
        this.successCount = successCount;
    }

    @Override
    public CircuitBreakerState nextState(Result result) {
        if (result.isOk()) {
            if (this.successCount + 1 >= this.threshold) {
                return new ClosedState();
            } else {
                return new HalfOpenState(successCount + 1);
            }
        } else {
            return new OpenState();
        }
    }
}
