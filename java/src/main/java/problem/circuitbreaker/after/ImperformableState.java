package problem.circuitbreaker.after;

public interface ImperformableState extends CircuitBreakerState {
    CircuitBreakerState nextState();
}
