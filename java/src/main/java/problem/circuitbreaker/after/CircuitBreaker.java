package problem.circuitbreaker.after;

import java.util.Optional;

public class CircuitBreaker {
    private CircuitBreakerState state;

    public CircuitBreaker() {
        state = new ClosedState();
    }

    private Result perform(Runnable action) {
        try {
            action.run();
            return Result.ok();
        } catch (Exception ex) {
            return Result.fail(ex);
        }
    }

    public void executeAction(Runnable action) {
        if (state instanceof PerformableState) {
            state = Optional.of(state)
                    .map(PerformableState.class::cast)
                    .map(state -> state.nextState(perform(action)))
                    .orElseThrow();
        } else if (state instanceof ImperformableState) {
            state = ((ImperformableState) state).nextState();
        }
    }
}
