package problem.circuitbreaker.after;

public class Result {
    private boolean success;
    private Exception exception;

    public static Result ok() {
        Result result = new Result();
        result.success = true;
        return result;
    }

    public static Result fail(Exception ex) {
        Result result = new Result();
        result.success = false;
        result.exception = ex;
        return result;
    }

    public boolean isOk() {
        return success;
    }

}
