package problem.circuitbreaker.after;

public class OpenState implements ImperformableState {
    private final long openedAt;
    private final long openToHalfOpenWaitTime;

    public OpenState() {
        this(System.currentTimeMillis(), 3000L);
    }

    public OpenState(long openedAt, long openToHalfOpenWaitTime) {
        this.openedAt = openedAt;
        this.openToHalfOpenWaitTime = openToHalfOpenWaitTime;
    }

    @Override
    public CircuitBreakerState nextState() {
        if (System.currentTimeMillis() - openedAt > openToHalfOpenWaitTime) {
            return new HalfOpenState();
        } else {
            return this;
        }
    }
}
