package problem.workflow.before;

import java.util.Set;

public class RequisitionException extends RuntimeException {
    public RequisitionException(String message) {
        super(message);
    }

    public RequisitionException(Set<String> messages) {
        super(String.join(",", messages));
    }
}
