package problem.workflow.before;

import java.time.LocalDateTime;

public class ApproveHistory {
    private boolean isDeclined;
    private Employee approver;
    private LocalDateTime approvedAt;

    public ApproveHistory(boolean isDeclined, Employee approver, LocalDateTime approvedAt) {
        this.isDeclined = isDeclined;
        this.approver = approver;
        this.approvedAt = approvedAt;
    }

    public boolean isDeclined() {
        return isDeclined;
    }

    public void setDeclined(boolean declined) {
        isDeclined = declined;
    }

    public Employee getApprover() {
        return approver;
    }

    public void setApprover(Employee approver) {
        this.approver = approver;
    }

    public LocalDateTime getApprovedAt() {
        return approvedAt;
    }

    public void setApprovedAt(LocalDateTime approvedAt) {
        this.approvedAt = approvedAt;
    }
}
