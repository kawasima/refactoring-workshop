package problem.workflow.before;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 購入申請.
 */
public class Requisition {
    /** 承認ステータス */
    private String requisitionStatus;

    /** 承認ルート */
    private final List<Employee> approveRoute;

    /** 購入商品 */
    private final List<RequisitionLine> requisitionLines;

    /** 承認履歴 */
    private final List<ApproveHistory> approveHistories;

    public Requisition(List<RequisitionLine> requisitionLines,
                       List<Employee> approveRoute) {
        this.requisitionLines = requisitionLines;
        if (approveRoute == null || approveRoute.isEmpty())
            throw new RequisitionException("承認ルートが設定されていません");
        this.approveRoute = approveRoute;
        approveHistories = new ArrayList<>();
        requisitionStatus = "APPLIED";
    }

    public void checkRoute() {
        BigDecimal total = requisitionLines.stream()
                .map(RequisitionLine::getPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        if (total.compareTo(new BigDecimal("200000")) <= 0) {
            if (!"課長".equals(approveRoute.get(approveRoute.size() - 1).getPosition())) {
                throw new RequisitionException("この申請は課長の最終承認が必要です");
            }
        } else if (total.compareTo(new BigDecimal("1000000")) <= 0) {
            if (!"部長".equals(approveRoute.get(approveRoute.size() - 1).getPosition())) {
                throw new RequisitionException("この申請は部長の最終承認が必要です");
            }
        } else {
            if (!"社長".equals(approveRoute.get(approveRoute.size() - 1).getPosition())) {
                throw new RequisitionException("この申請は社長の最終承認が必要です");
            }
        }
    }

    public String getStatus() {
        if (requisitionStatus.equals("APPLIED") || requisitionStatus.equals("DECLINED")) {
            return requisitionStatus;
        }
        ApproveHistory lastHistory = approveHistories.isEmpty() ? null : approveHistories.get(approveHistories.size() - 1);
        int routeIndex = 0;
        for (; routeIndex < approveRoute.size(); routeIndex++) {
            if (lastHistory.getApprover().getId().equals(approveRoute.get(routeIndex).getId())) {
                break;
            }
        }
        if (routeIndex >= approveRoute.size() - 1) {
            return "FINAL_ACCEPTED";
        }
        return requisitionStatus;
    }

    public void proceedWorkflow(Employee approver, boolean isDeclined) {
        if (isDeclined && requisitionStatus.equals("DECLINED")) {
            throw new RequisitionException("すでに差し戻されている申請です");
        }
        ApproveHistory lastHistory = approveHistories.isEmpty() ? null : approveHistories.get(approveHistories.size() - 1);
        int routeIndex = -1;
        if (lastHistory != null) {
            for (routeIndex = 0; routeIndex < approveRoute.size(); routeIndex++) {
                if (lastHistory.getApprover().getId().equals(approveRoute.get(routeIndex).getId())) {
                    break;
                }
            }
            if (routeIndex >= approveRoute.size()) {
                routeIndex = -1;
            }
        }
        if (requisitionStatus.equals("ACCEPTED") && routeIndex >= approveRoute.size() - 1) {
            throw new RequisitionException("申請はすでに最終承認されています");
        }

        if (lastHistory != null && lastHistory.isDeclined()) {
            if (isDeclined) {
                throw new RequisitionException("すでに否認された申請です");
            }
            if (!approver.getId().equals(approveRoute.get(0).getId())) {
                throw new RequisitionException("承認者が承認ルートで設定されたものと違います");
            }
        } else {
            if (!approver.getId().equals(approveRoute.get(routeIndex + 1).getId())) {
                throw new RequisitionException("承認者が承認ルートで設定されたものと違います");
            }
        }

        requisitionStatus = isDeclined ? "DECLINED" : "ACCEPTED";

        approveHistories.add(new ApproveHistory(
                isDeclined,
                approver,
                LocalDateTime.now()
        ));
    }
}
