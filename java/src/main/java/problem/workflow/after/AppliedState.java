package problem.workflow.after;

public class AppliedState implements RequisitionState, Approvable, Declinable {
    private final Employee lastApprover;

    public AppliedState() {
        this.lastApprover = null;
    }
    public AppliedState(Employee approver) {
        this.lastApprover = approver;
    }

    @Override
    public AppliedState approve(Employee approver) {
        return new AppliedState(approver);
    }

    @Override
    public FinalApprovedState approveFinal(Employee approver) {
        return new FinalApprovedState();
    }

    @Override
    public DeclinedState decline() {
        return new DeclinedState();
    }

    @Override
    public Employee getLastApprover() {
        return lastApprover;
    }
}
