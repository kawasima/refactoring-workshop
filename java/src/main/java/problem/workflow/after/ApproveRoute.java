package problem.workflow.after;

import problem.workflow.before.RequisitionException;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ApproveRoute {
    private final LinkedList<Employee> approvers;

    public ApproveRoute(List<Employee> approvers) {
        if (approvers == null || approvers.isEmpty())
            throw new RequisitionException("");
        this.approvers = new LinkedList<>(approvers);
    }
    public Employee getFinalApprover() {
        return approvers.getLast();
    }

    public Optional<Employee> nextApprover(Employee prevApprover) {
        if (prevApprover == null) {
            return Optional.of(approvers.get(0));
        }

        return IntStream.range(0, approvers.size() - 1)
                .filter(i -> approvers.get(i).equals(prevApprover))
                .mapToObj(i -> approvers.get(i+1))
                .findFirst();
    }

    public boolean isFinalApprover(Employee approver) {
        return getFinalApprover().equals(approver);
    }


    public Stream<Employee> stream() {
        return approvers.stream();
    }

    public int size() {
        return approvers.size();
    }

    public Employee get(int i) {
        return approvers.get(i);
    }
}
