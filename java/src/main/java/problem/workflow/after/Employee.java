package problem.workflow.after;

import java.util.Optional;

public class Employee {
    private final String id;
    private final Position position;

    public Employee(String id, Position position) {
        this.id = id;
        this.position = position;
    }

    @Override
    public boolean equals(Object another) {
        return Optional.ofNullable(another)
                .filter(Employee.class::isInstance)
                .map(Employee.class::cast)
                .filter(emp -> emp.id.equals(id))
                .isPresent();
    }

    public Position getPosition() {
        return position;
    }
}
