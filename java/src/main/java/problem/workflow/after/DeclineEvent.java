package problem.workflow.after;

import java.time.LocalDateTime;

public class DeclineEvent extends WorkflowEvent {
    private final Employee decliner;

    protected DeclineEvent(Employee decliner) {
        super(LocalDateTime.now());
        this.decliner = decliner;
    }

    public Employee getDecliner() {
        return decliner;
    }
}
