package problem.workflow.after;

import java.time.LocalDateTime;

public abstract class WorkflowEvent {
    private final LocalDateTime createdAt;

    protected WorkflowEvent(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }
}
