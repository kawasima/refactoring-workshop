package problem.workflow.after;

public interface Declinable {
    DeclinedState decline();
    Employee getLastApprover();
}
