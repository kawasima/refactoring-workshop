package problem.workflow.after;

import java.time.LocalDateTime;

public class ApproveEvent extends WorkflowEvent {
    private final Employee approver;

    public ApproveEvent(Employee approver) {
        super(LocalDateTime.now());
        this.approver = approver;
    }

    public Employee getApprover() {
        return approver;
    }
}
