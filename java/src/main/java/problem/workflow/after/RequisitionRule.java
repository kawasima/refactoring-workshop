package problem.workflow.after;

import java.util.Set;

public interface RequisitionRule {
    Set<String> validate(Requisition requisition);
}
