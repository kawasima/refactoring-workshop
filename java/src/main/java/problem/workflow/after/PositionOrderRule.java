package problem.workflow.after;

import java.util.Collections;
import java.util.Set;
import java.util.stream.IntStream;

public class PositionOrderRule implements RequisitionRule {
    @Override
    public Set<String> validate(Requisition requisition) {
        ApproveRoute approveRoute = requisition.getApproveRoute();
        boolean isValid = IntStream.range(1, approveRoute.size())
                .allMatch(i -> approveRoute.get(i).getPosition().greaterEqualThan(approveRoute.get(i-1).getPosition()));

        return isValid ? Collections.emptySet() : Set.of("承認者順は役職の低い方から高い方へと並んでなくはなりません");

    }
}
