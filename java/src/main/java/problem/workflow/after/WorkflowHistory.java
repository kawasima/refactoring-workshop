package problem.workflow.after;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class WorkflowHistory {
    private final List<WorkflowEvent> workflowEvents;

    public WorkflowHistory() {
        workflowEvents = new ArrayList<>();
    }

    public void onApprove(Employee approver) {
        workflowEvents.add(new ApproveEvent(approver));
    }

    public void onDecline(Employee decliner) {
        workflowEvents.add(new DeclineEvent(decliner));
    }

    public Stream<WorkflowEvent> stream() {
        return workflowEvents.stream();
    }
}
