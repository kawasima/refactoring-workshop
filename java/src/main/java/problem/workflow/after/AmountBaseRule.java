package problem.workflow.after;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Set;

public class AmountBaseRule implements RequisitionRule {
    @Override
    public Set<String> validate(Requisition requisition) {
        BigDecimal total = requisition.getRequisitionLines().stream()
                .map(RequisitionLine::getPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        Employee finalApprover = requisition.getApproveRoute().getFinalApprover();

        if (total.compareTo(new BigDecimal("200000")) <= 0) {
            if (!finalApprover.getPosition().equals(Position.課長)) {
                return Set.of("この申請は課長の最終承認が必要です");
            }
        } else if (total.compareTo(new BigDecimal("1000000")) <= 0) {
            if (!finalApprover.getPosition().equals(Position.部長)) {
                return Set.of("この申請は課長の最終承認が必要です");
            }
        } else {
            if (!finalApprover.getPosition().equals(Position.社長)) {
                return Set.of("この申請は課長の最終承認が必要です");
            }
        }
        return Collections.emptySet();
    }
}
