package problem.workflow.after;

import problem.workflow.before.RequisitionException;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Requisition {
    private RequisitionState requisitionState;

    private final ApproveRoute approveRoute;
    private final List<RequisitionLine> requisitionLines;
    private final WorkflowHistory workflowHistory;

    private static final Set<RequisitionRule> DEFAULT_REQUISITION_RULES = Set.of(
            new AmountBaseRule(),
            new PositionOrderRule()
    );
    private final Set<RequisitionRule> requisitionRules;

    public Requisition(List<RequisitionLine> requisitionLines,
                       ApproveRoute approveRoute) {
        this.requisitionLines = requisitionLines;
        this.approveRoute = approveRoute;
        workflowHistory = new WorkflowHistory();
        requisitionState = new DraftState();
        requisitionRules = DEFAULT_REQUISITION_RULES;
        validate();
    }

    protected void validate() {
        Set<String> messages = requisitionRules.stream()
                .flatMap(rule -> rule.validate(this).stream())
                .collect(Collectors.toSet());
        if (!messages.isEmpty()) {
            throw new RequisitionException(messages);
        }
    }
    public void applied() {
        if (requisitionState instanceof Appliable) {
            requisitionState = ((Appliable) requisitionState).apply();
        } else {
            throw new RequisitionException("ワークフローは申請できない状態です");
        }
    }

    public void approve(Employee approver) {
        if (requisitionState instanceof Approvable) {
            Approvable approvable = (Approvable) requisitionState;
            Employee nextApprover = approveRoute.nextApprover(approvable.getLastApprover())
                    .filter(approver::equals)
                    .orElseThrow(() -> new RequisitionException("承認ルートと合致しない承認者です"));

            requisitionState = approveRoute.isFinalApprover(nextApprover) ?
                    approvable.approveFinal(nextApprover)
                    :
                    approvable.approve(nextApprover);
            workflowHistory.onApprove(approver);
        } else {
            throw new RequisitionException("ワークフローは承認できない状態です");
        }
    }

    public void decline(Employee decliner) {
        if (requisitionState instanceof Declinable) {
            Declinable declinable = (Declinable) requisitionState;
            Employee nextApprover = approveRoute.nextApprover(declinable.getLastApprover())
                    .filter(decliner::equals)
                    .orElseThrow(() -> new RequisitionException("承認ルートと合致しない承認者です"));

            requisitionState = declinable.decline();
            workflowHistory.onDecline(decliner);
        } else {
            throw new RequisitionException("ワークフローは否認できない状態です");
        }
    }

    public ApproveRoute getApproveRoute() {
        return approveRoute;
    }

    public List<RequisitionLine> getRequisitionLines() {
        return requisitionLines;
    }

    public RequisitionState getRequisitionState() {
        return requisitionState;
    }
}
