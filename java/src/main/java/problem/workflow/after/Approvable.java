package problem.workflow.after;

public interface Approvable {
    AppliedState approve(Employee approver);
    FinalApprovedState approveFinal(Employee approver);
    Employee getLastApprover();
}
