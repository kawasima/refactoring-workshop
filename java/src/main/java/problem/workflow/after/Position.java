package problem.workflow.after;

public enum Position {
    主任("主任", 10),
    課長("課長", 20),
    部長("部長", 30),
    社長("社長", 40),
    ;

    private final String name;
    private final int rank;

    Position(String name, int rank) {
        this.name = name;
        this.rank = rank;
    }

    public String getName() {
        return name;
    }

    public boolean greaterEqualThan(Position another) {
        return this.rank >= another.rank;
    }
}
