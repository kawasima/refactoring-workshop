package problem.circuitbreaker.after;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class CircuitBreakerTest {
    @Test
    void closeToClose() {
        long current = System.currentTimeMillis();
        ClosedState state = new ClosedState(List.of(
                current, current+1
        ));
        state.setThreshold(4);
        assertThat(state.nextState(Result.fail(new Exception())))
                .isInstanceOf(ClosedState.class);
    }

    @Test
    void closedToOpen() {
        long current = System.currentTimeMillis();
        ClosedState state = new ClosedState(List.of(
                current, current+1
        ));
        state.setThreshold(3);
        assertThat(state.nextState(Result.fail(new Exception())))
                .isInstanceOf(OpenState.class);
    }

    @Test
    void openToOpen() {
        long current = System.currentTimeMillis();
        var state = new OpenState(current, 1000L);
        assertThat(state.nextState()).isInstanceOf(OpenState.class);
    }

    @Test
    void openToHalfOpen() {
        long current = System.currentTimeMillis();
        var state = new OpenState(current - 1010, 1000L);
        assertThat(state.nextState()).isInstanceOf(HalfOpenState.class);
    }

}
