package problem.circuitbreaker.before;

import org.junit.jupiter.api.Test;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

class CircuitBreakerTest {
    @Test
    void randomErrors() {
        CircuitBreaker circuitBreaker = new CircuitBreaker();
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(1);
        do {
            try {
                TimeUnit.MICROSECONDS.sleep(100);
                circuitBreaker.executeAction(() -> {
                    if (Math.random() > 0.5) {
                        throw new RuntimeException("error");
                    }
                });
            } catch (Exception e) {
                // Do nothing
            }
        } while (!circuitBreaker.getStatus().equals("open"));

        assertThat(circuitBreaker.getStatus()).isEqualTo("open");

        do {
            try {
                TimeUnit.MICROSECONDS.sleep(100);
                circuitBreaker.executeAction(() -> {
                    if (Math.random() > 0.5) {
                        throw new RuntimeException("error");
                    }
                });
            } catch (Exception e) {
                // Do nothing
            }
        } while (!circuitBreaker.getStatus().equals("half_open"));

        assertThat(circuitBreaker.getStatus()).isEqualTo("half_open");

        do {
            try {
                TimeUnit.MICROSECONDS.sleep(100);
                circuitBreaker.executeAction(() -> {
                });
            } catch (Exception e) {
                // Do nothing
            }
        } while (!circuitBreaker.getStatus().equals("close"));

        executorService.shutdown();
    }
}
