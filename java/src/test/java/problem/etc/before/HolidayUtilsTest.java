package problem.etc.before;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Month;

import static org.assertj.core.api.Assertions.assertThat;

class HolidayUtilsTest {
    @Test
    void workday20230126() {
        assertThat(HolidayUtils.isHoliday(LocalDate.of(2023, Month.JANUARY, 26)))
                .isFalse();
    }
    @Test
    void holiday20230128_saturday() {
        assertThat(HolidayUtils.isHoliday(LocalDate.of(2023, Month.JANUARY, 28)))
                .isTrue();
    }
    @Test
    void holiday20230129_sunday() {
        assertThat(HolidayUtils.isHoliday(LocalDate.of(2023, Month.JANUARY, 29)))
                .isTrue();
    }

    @Test
    void holiday20230102_monday_but_new_year() {
        assertThat(HolidayUtils.isHoliday(LocalDate.of(2023, Month.JANUARY, 2)))
                .isTrue();
    }
}
