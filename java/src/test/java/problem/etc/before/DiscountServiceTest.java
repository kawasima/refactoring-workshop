package problem.etc.before;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import problem.etc.before.model.HighwayDrive;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

class DiscountServiceTest {
    DiscountService sut;

    @BeforeEach
    void setUp() {
        sut = new DiscountService();
    }

    @Test
    public void 平日朝夕は朝夕割が適用される() {
        HighwayDrive drive = new HighwayDrive();
        drive.setEnteredAt(LocalDateTime.of(2016, 3, 31, 23, 0));
        drive.setExitedAt(LocalDateTime.of(2016, 4, 1, 6, 30));
        drive.setCountPerMonth(10);
        drive.setVehicleFamily("standard");
        drive.setRouteType("rural");

        assertThat(sut.calc(drive)).isEqualTo(50);
    }

    @Test
    public void 休日朝夕は休日割が適用される() {
        HighwayDrive drive = new HighwayDrive();
        drive.setEnteredAt(LocalDateTime.of(2016, 4, 1, 23, 0));
        drive.setExitedAt(LocalDateTime.of(2016, 4, 2, 6, 30));
        drive.setCountPerMonth(10);
        drive.setVehicleFamily("standard");
        drive.setRouteType("rural");

        assertThat(sut.calc(drive)).isEqualTo(30);
    }

}
