package problem.workflow.before;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class RequisitionTest {
    private final Employee 主任 = new Employee("1", "主任");
    private final Employee 課長 = new Employee("2", "課長");
    private final Employee 部長 = new Employee("3", "部長");
    private final Employee 社長 = new Employee("4", "社長");

    @Test
    void 課長承認で課長が一度差し戻す() {
        Requisition requisition = new Requisition(
                List.of(new RequisitionLine("タブレット", new BigDecimal("15000"))),
                List.of(主任, 課長)
        );

        requisition.checkRoute();
        assertThat(requisition.getStatus()).isEqualTo("APPLIED");

        requisition.proceedWorkflow(主任, false);
        assertThat(requisition.getStatus()).isEqualTo("ACCEPTED");

        requisition.proceedWorkflow(課長, true);
        assertThat(requisition.getStatus()).isEqualTo("DECLINED");

        requisition.proceedWorkflow(主任, false);
        assertThat(requisition.getStatus()).isEqualTo("ACCEPTED");

        requisition.proceedWorkflow(課長, false);
        assertThat(requisition.getStatus()).isEqualTo("FINAL_ACCEPTED");
    }

    @Test
    void 総額30万円の決裁は部長承認() {
        Requisition requisition = new Requisition(
                List.of(new RequisitionLine("パソコン", new BigDecimal("290000")),
                        new RequisitionLine("キーボード", new BigDecimal("10000"))),
                List.of(主任, 課長)
        );
        assertThatThrownBy(() -> requisition.checkRoute())
                .satisfies(ex -> ex.getMessage().equals("この申請は部長の最終承認が必要です"));
    }

    @Test
    void 総額100万円超の決裁は社長承認() {
        Requisition requisition = new Requisition(
                List.of(new RequisitionLine("パソコンA", new BigDecimal("300000")),
                        new RequisitionLine("パソコンB", new BigDecimal("300000")),
                        new RequisitionLine("パソコンC", new BigDecimal("400001"))),
                List.of(主任, 課長)
        );
        assertThatThrownBy(() -> requisition.checkRoute())
                .satisfies(ex -> ex.getMessage().equals("この申請は社長の最終承認が必要です"));
    }
}
