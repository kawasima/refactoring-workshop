package problem.workflow.after;

import org.junit.jupiter.api.Test;
import problem.workflow.before.RequisitionException;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class RequisitionTest {
    private final Employee 主任 = new Employee("1", Position.主任);
    private final Employee 課長 = new Employee("2", Position.課長);
    private final Employee 部長 = new Employee("3", Position.部長);
    private final Employee 社長 = new Employee("4", Position.社長);

    @Test
    void 課長承認で課長が一度差し戻す() {
        Requisition requisition = new Requisition(
                List.of(new RequisitionLine("タブレット", new BigDecimal("15000"))),
                new ApproveRoute(List.of(主任, 課長))
        );

        requisition.applied();
        requisition.approve(主任);
        assertThat(requisition.getRequisitionState()).isInstanceOf(AppliedState.class);

        requisition.decline(課長);
        assertThat(requisition.getRequisitionState()).isInstanceOf(DeclinedState.class);

        requisition.applied();
        requisition.approve(主任);
        assertThat(requisition.getRequisitionState()).isInstanceOf(AppliedState.class);

        requisition.approve(課長);
        assertThat(requisition.getRequisitionState()).isInstanceOf(FinalApprovedState.class);
    }

    @Test
    void 役職のひっくり返った承認ルート() {
        assertThatThrownBy(() -> new Requisition(
                List.of(new RequisitionLine("タブレット", new BigDecimal("15000"))),
                new ApproveRoute(List.of(課長, 主任))))
                .isInstanceOf(RequisitionException.class)
                .hasMessageContaining("承認者順は役職の低い方から高い方へと並んでなくはなりません");
    }
}
