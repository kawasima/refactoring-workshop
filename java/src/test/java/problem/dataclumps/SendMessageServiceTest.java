package problem.dataclumps;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import lesson.dataclumps.Contact;
import lesson.dataclumps.SendMessageService;
import org.assertj.core.groups.Tuple;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;

import static org.assertj.core.api.Assertions.assertThat;

class SendMessageServiceTest {
    SendMessageService sut;
    ListAppender<ILoggingEvent> listAppender;

    @BeforeEach
    void setup() {
        sut = new SendMessageService();
        listAppender = new ListAppender<>();
        listAppender.start();
        Logger logger = (Logger) LoggerFactory.getLogger(SendMessageService.class);
        logger.addAppender(listAppender);
    }
    @Test
    void sendMessageViaDM() {
        Contact contact = new Contact();
        contact.setZip("1234567");
        contact.setState("Tokyo");
        sut.send(contact);
        assertThat(listAppender.list)
                .extracting(ILoggingEvent::getFormattedMessage, ILoggingEvent::getLevel)
                .containsExactly(Tuple.tuple("Send a DM to 1234567 Tokyo null null null", Level.INFO));
    }


    @Test
    void sendMessageViaEmail() {
        Contact contact = new Contact();
        contact.setEmailAddress("abc@example.com");
        contact.setEmailVerified(true);
        sut.send(contact);
        assertThat(listAppender.list)
                .extracting(ILoggingEvent::getFormattedMessage, ILoggingEvent::getLevel)
                .containsExactly(Tuple.tuple("Send an email to abc@example.com", Level.INFO));
    }
    @Test
    void doesNotSendMessageUnverifiedAddress() {
        Contact contact = new Contact();
        contact.setEmailAddress("abc@example.com");
        sut.send(contact);
        assertThat(listAppender.list)
                .extracting(ILoggingEvent::getFormattedMessage, ILoggingEvent::getLevel)
                .isEmpty();
    }
}
