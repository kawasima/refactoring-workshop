package problem.featureenvy;

import lesson.featureenvy.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.assertj.core.api.Assertions.assertThat;

class CartPricerTest {
    TaxManager taxManager;
    User user1;
    SKU apple;
    SKU orange;

    @BeforeEach
    public void setup() {
        taxManager = new TaxManager();
        taxManager.add("JP", new BigDecimal("1.10"));
        taxManager.add("FR", new BigDecimal("1.20"));
        user1 = new User("1", "JP");
        apple = new SKU("1", "Apple", 100);
        orange = new SKU("2", "Orange", 30);
    }

    @Test
    void calculateTax() {
        Cart cart = new Cart(user1);
        cart.add(apple, 3);
        cart.add(orange, 4);
        BigDecimal total = new CartPricer(taxManager).total(cart);
        assertThat(total.setScale(2, RoundingMode.HALF_UP))
                .isEqualTo(new BigDecimal("462").setScale(2, RoundingMode.HALF_UP));
    }
}
