package lesson.decomposeconditional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

class ChargerAfterTest {
    Plan plan;

    @BeforeEach
    void setup() {
        plan = new Plan(
                new ComparableDate(2022, 6, 1),
                new ComparableDate(3033, 8, 31),
                new BigDecimal("1.08"),
                new BigDecimal("1.1"),
                new BigDecimal("100")
        );
    }
    @Test
    void summerRate() {
        assertThat(ChargerAfter.charge(10, new ComparableDate(2022, 7,1), plan))
                .isEqualTo(new BigDecimal("10.8"));
    }

    @Test
    void regularRate() {
        assertThat(ChargerAfter.charge(10, new ComparableDate(2022, 9, 1), plan))
                .isEqualTo(new BigDecimal("11"));
    }
}
